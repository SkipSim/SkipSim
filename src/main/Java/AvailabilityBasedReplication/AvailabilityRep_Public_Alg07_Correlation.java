//import java.util.Random;
//
///**
// * Created by Yahya on 5/23/2016.
// */
//public class AvailabilityRep_Public_Alg07_Correlation
//{
//    public static void randomReplicaGenerator()
//    {
//        int repCounter = 0;
//
//
//        while(repCounter < Simulator.system.getReplicationDegree())
//        {
//            /*
//            Finding the most available replica
//             */
//            double maxAvailability = 0;
//            int maxIndex = 0;
//            for (int i = 0; i < Simulator.system.getNumIDSeed(); i++)
//            {
//                if(!Developments.clustering.getCluster(i))
//                    continue;
//                double totalAvailability = DataTypes.nodesTimeTable.totalAvailabilityChanceOfThisNumId(i);
//                if (totalAvailability > maxAvailability && !repTools.getDynamicRealWorldReplicaSet(i) )
//                {
//                    maxAvailability = totalAvailability;
//                    maxIndex = i;
//                }
//            }
//            repTools.setDeyamicRealWorldReplicaSet(maxIndex, true);
//            repCounter++;
//
//            /*
//            Finding the best anti-correlated candidate
//             */
//            double maxCorrelation = 0;
//            int maxCorrelationIndex = 0;
//            for (int i = 0; i < Simulator.system.getNumIDSeed(); i++)
//            {
//                if(!Developments.clustering.getCluster(i))
//                    continue;
//
//                if(repTools.getDynamicRealWorldReplicaSet(i))
//                    continue;
//                double correlation = DataTypes.nodesTimeTable.correlation(maxIndex, i);
//                double availability = DataTypes.nodesTimeTable.totalAvailabilityChanceOfThisNumId(i);
//                if(correlation > 0 && (availability/(Math.pow(correlation, 2)) > maxCorrelation))
//                {
//                    maxCorrelation = (availability/Math.pow(correlation, 2));
//                    maxCorrelationIndex = i;
//                }
//
//            }
//
//
//            repTools.setDeyamicRealWorldReplicaSet(maxCorrelationIndex, true);
//            repCounter++;
//
//
//            System.out.println("A replication on a set of anti correlated SkipGraph.Node has been done:");
//            System.out.println("NumID " + maxIndex);
//            for (int t = 0; t < repTools.getTimeSlots(); t++)
//            {
//                double prob =  (DataTypes.nodesTimeTable.getAvailabilityProbability(maxIndex, t));
//                System.out.print(String.format( "%.2f", prob) + " ");
//            }
//
//            System.out.println();
//            System.out.println("NumID " + maxCorrelationIndex);
//            for (int t = 0; t < repTools.getTimeSlots(); t++)
//            {
//                double prob =  (DataTypes.nodesTimeTable.getAvailabilityProbability(maxCorrelationIndex, t));
//                System.out.print(String.format( "%.2f", prob) + " ");
//            }
//            System.out.println();
//            System.out.println();
//
//
//        }
//
//        System.out.println("Replication is done");
//
//    }
//
//
//
//
//    public static void Algorithm(boolean boost)
//    {
//        System.out.println("Correlation based replication has been started");
//
//        repTools.reset();
//        repTools.tablesInit();
//        repTools.dynamicReplicaSetInit();
//        Developments.clustering.reset();
//
//        if(boost)
//            {
//                System.out.println("Boosted correlation is fired up!");
//                double[] availability = new double[Simulator.system.getNumIDSeed()];
//                for(int i = 0 ; i < Simulator.system.getNumIDSeed() ; i++)
//                    {
//                        availability[i] = DataTypes.nodesTimeTable.totalAvailabilityChanceOfThisNumId(i);
//                    }
//                Developments.clustering.Developments.clustering(availability);
//            }
//        randomReplicaGenerator();
//        predictedAvilabilityPerHour();
//
//
//    }
//
//    private static void predictedAvilabilityPerHour()
//    {
//        double totalAvailability = 0;
//        for(int t = 0 ; t < repTools.getTimeSlots() ; t++)
//            for(int i = 0 ; i < Simulator.system.getNumIDSeed() ; i++)
//            {
//                if(repTools.getDynamicRealWorldReplicaSet(i))
//                {
//                    totalAvailability += DataTypes.nodesTimeTable.getAvailabilityProbability(i, t);
//                }
//            }
//
//        System.out.println("Predicted availability probability per hour is " + totalAvailability);
//    }
//}
