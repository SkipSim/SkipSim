package AvailabilityBasedReplication;

import Simulator.SkipSimParameters;
import SkipGraph.Node;
import SkipGraph.Nodes;

import java.util.Random;

public class AvailabilityRep_Public_Alg01_Randomized extends AvailavilityBasedReplication
{
    @Override
    public void Algorithm(int dataOwnerIndex, int replicationDegree, Nodes ns)
    {
        Random random = new Random();

        int repCounter = 0;
        while (true)
        {
            int replica = random.nextInt(SkipSimParameters.getSystemCapacity() - 1);
            if (!((Node) ns.getNode(replica)).getReplicaIDSet().contains(dataOwnerIndex) && ((Node) ns.getNode(replica)).isOnline())
            {
                ((Node) ns.getNode(replica)).getReplicaIDSet().add(dataOwnerIndex);
                System.out.println(" data owner index " + dataOwnerIndex + " replica index" + replica);
                repCounter++;
                if(repCounter >= replicationDegree) break;
            }
        }

    }
}