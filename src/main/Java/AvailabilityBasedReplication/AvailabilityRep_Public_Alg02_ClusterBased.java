//import java.util.Random;
//
//public class AvailabilityRep_Public_Alg02_ClusterBased
//{
//	private static int[] clusterID;
//	private static int[] clusterSize;
//
//
//    private static void randomReplicaGenerator()
//    {
//		//System.out.println("Available SkipGraph.Nodes: " + SkipGraph.Nodes.numOfNonEmptyNodes());
//		/*
//		Finding Replication Quota of Each Cluster
//		 */
//		double repUnit = (double) Simulator.system.getReplicationDegree() / Simulator.system.getNumIDSeed();
//		int[] clusterQuota = new int[Simulator.system.getReplicationDegree()];
//		int QuotaSum = 0;
//		int maxQuota = 0;
//		int maxIndex = 0;
//		for(int i = 0 ; i < Simulator.system.getReplicationDegree() ; i++)
//			{
//				clusterQuota[i] = (int)(repUnit * clusterSize[i]);
//				if(clusterQuota[i] > maxQuota)
//					{
//						maxQuota = clusterQuota[i];
//						maxIndex = i;
//					}
//				QuotaSum += clusterQuota[i];
//			}
//
//		if(QuotaSum < Simulator.system.getReplicationDegree())
//			{
//				clusterQuota[maxIndex] += (Simulator.system.getReplicationDegree() - QuotaSum);
//			}
//
//		/*
//		Replicate in each group based on quota
//		 */
//		for(int i = 0; i < Simulator.system.getReplicationDegree() ; i++)
//			{
//				if(clusterQuota[i] == 0)
//					continue;
//				int repCounter = 0;
//				while (repCounter < clusterQuota[i])
//					{
//						double maxAvailability = 0;
//						int maxAvailabilityIndex = 0;
//						for(int j = 0 ; j < Simulator.system.getSystemCapacity(); j++)
//							{
//								if(!Developments.clustering.getCluster(j))
//									continue;
//								if(clusterID[j] != i)
//									continue;
//								double a = DataTypes.nodesTimeTable.totalAvailabilityChanceOfThisNumId(j);
//								if(a > maxAvailability && !repTools.getDynamicRealWorldReplicaSet(j))
//									{
//										maxAvailability = a;
//										maxAvailabilityIndex = j;
//									}
//							}
//
//						repTools.setDeyamicRealWorldReplicaSet(maxAvailabilityIndex,true);
//						//System.out.println("Replica " + maxAvailabilityIndex);
//						repCounter++;
//					}
//			}
//
//
//		System.out.println("Replication is done");
//   	     
//
//   	     
//   	     
//
//   	 }
//
//   		 
//   
//	 
//	 public static void Algorithm(boolean boost)
//	 {
//		   //System.out.println("Dynamic Public Availability Based Randomized Replication at time " + time);
//
//
//		   repTools.reset();
//		   repTools.tablesInit();
//		   repTools.dynamicReplicaSetInit();
//		   Developments.clustering.reset();
//		   if(boost)
//				 {
//					 System.out.println("Boosted cluster is fired up!");
//					 double[] availability = new double[Simulator.system.getNumIDSeed()];
//					 for(int i = 0 ; i < Simulator.system.getNumIDSeed() ; i++)
//						 {
//							 availability[i] = DataTypes.nodesTimeTable.totalAvailabilityChanceOfThisNumId(i);
//						 }
//					 Developments.clustering.Developments.clustering(availability);
//				 }
//	       Developments.clustering();
//		   randomReplicaGenerator();
//		   //predictedAvilabilityPerHour();
//		  
//        
//	 }
//
//	private static void Developments.clustering()
//	{
//		/*
//		Initialiizing MNR cluster with MNR randomized centeroid
//		 */
//		clusterID = new int[Simulator.system.getNumIDSeed()];
//		clusterSize = new int[Simulator.system.getReplicationDegree()];
//		double[][] clusterCenteroids = new double[Simulator.system.getReplicationDegree()][repTools.getTimeSlots()];
//		Random random = new Random();
//		for(int i = 0 ; i < Simulator.system.getReplicationDegree() ; i++)
//		{
//			for(int t = 0 ; t < repTools.getTimeSlots() ; t++)
//				{
//					clusterCenteroids[i][t] = random.nextDouble();
//					//System.out.print(String.format( "%.2f", clusterCenteroids[i][t]) + "\t");
//				}
//			//System.out.println();
//		}
//
//
//
//		/*
//		Rest of the SkipGraph.Nodes' cluster IDs are initilized with -1
//		 */
//		for(int i = 0; i < Simulator.system.getNumIDSeed() ; i++)
//			clusterID[i] = -1;
//
//		int iterations = 0;
//		while(true)
//			{
//				/*
//				Initial Clustering
//				 */
//				int clusterChanges = 0;
//				for (int i = 0; i < Simulator.system.getNumIDSeed(); i++)
//					{
//						if(!Developments.clustering.getCluster(i))
//							continue;
//						double minDistance = Double.MAX_VALUE;
//						int minIndex = 0;
//						for (int j = 0; j < Simulator.system.getReplicationDegree(); j++)
//						{
//							double distance = 0;
//							for (int t = 0; t < repTools.getTimeSlots(); t++)
//								{
//									double ds = DataTypes.nodesTimeTable.getAvailabilityProbability(i, t) - clusterCenteroids[j][t];
//									distance += Math.pow(ds, 2);
//						        }
//
//							if (distance < minDistance)
//								{
//									minDistance = distance;
//									minIndex = j;
//								}
//						}
//						if (clusterID[i] != minIndex)
//							{
//								clusterID[i] = minIndex;
//								clusterChanges++;
//							}
//					}
//
//				if (clusterChanges == 0)
//					{
//						System.out.println("Clustering has been done after " + iterations + " iterations");
//						break;
//					}
//				else
//					iterations++;
//
//				/*
//				Computing new cluster centroids
//				 */
//
//				for (int i = 0; i < Simulator.system.getReplicationDegree(); i++)
//					{
//						clusterSize[i] = 0;
//						for (int t = 0; t < repTools.getTimeSlots(); t++)
//							{
//							   clusterCenteroids[i][t] = 0;
//							}
//					}
//
//				for (int i = 0; i < Simulator.system.getNumIDSeed(); i++)
//					{
//						if(!Developments.clustering.getCluster(i))
//							continue;
//						for (int t = 0; t < repTools.getTimeSlots(); t++)
//						{
//							clusterCenteroids[clusterID[i]][t] += DataTypes.nodesTimeTable.getAvailabilityProbability(i, t);
//						}
//						clusterSize[clusterID[i]]++;
//					}
//
//				//System.out.println("Centroid Updates:");
//				for (int i = 0; i < Simulator.system.getReplicationDegree(); i++)
//					{
//						if(clusterSize[i] == 0)
//							continue;
//						for (int t = 0; t < repTools.getTimeSlots(); t++)
//							{
//								clusterCenteroids[i][t] /= clusterSize[i];
//								//System.out.print(String.format( "%.2f", clusterCenteroids[i][t]) + "\t");
//							}
//						//System.out.print(" size " + clusterSize[i] + " index " + i);
//						//System.out.println();
//					}
//		}
//
//
//
//
//
//	}
//
//
//
//	private static void predictedAvilabilityPerHour()
//	{
//		double totalAvailability = 0;
//		for(int t = 0 ; t < repTools.getTimeSlots() ; t++)
//			for(int i = 0 ; i < Simulator.system.getNumIDSeed() ; i++)
//			{
//				if(repTools.getDynamicRealWorldReplicaSet(i))
//				{
//					totalAvailability += DataTypes.nodesTimeTable.getAvailabilityProbability(i, t);
//				}
//			}
//
//		System.out.println("Predicted availability probability per hour is " + totalAvailability);
//	}
//}