package AvailabilityBasedReplication;

import SkipGraph.Nodes;

public abstract class AvailavilityBasedReplication
{
    public abstract void Algorithm(int dataOwnerIndex, int replicationDegree, Nodes ns);
}
