package SimulationSchema;

import DataTypes.Constants;
import Simulator.SkipSimParameters;

public class MultiObjectiveReplication extends SkipSimParameters
{
    public MultiObjectiveReplication()
    {
        //LOG = true;
        /*
        Replication
         */
        sDataOwnerNumber = 1;
        TopologyNumbers = 1;
        LifeTime = 30;
        SimulationType = Constants.SimulationType.DYNAMIC;
        ReplicationType = Constants.Replication.Type.PUBLIC;
        ReplicationTime = 1;
        ReplicationAlgorithm = Constants.Replication.Algorithms.RANDOMIZED;
        /*
        Churn model fast debian, with each region its own churn
         */
        SessionLengthScaleParameter = Constants.Churn.Model.Debian.Fast.SessionLength.Scale;
        SessionLengthShapeParameter = Constants.Churn.Model.Debian.Fast.SessionLength.Shape;
        InterarrivalScaleParameter = Constants.Churn.Model.Debian.Fast.SessionInterarrival.Scale;
        InterarrivalShapeParameter = Constants.Churn.Model.Debian.Fast.SessionInterarrival.Shape;
        MultipleInterArrivalDistribution = true;

    }
}
