package Simulator;

import LightChain.Transaction;
import ChurnStabilization.ChurnStochastics;
import ChurnStabilization.Interlace;
import ChurnStabilization.LookupEvaluation;
import DataBase.ChurnDBEntery;
import DataTypes.Constants;
import SkipGraph.Node;
import SkipGraph.SkipGraphOperations;
import StaticReplication.repEvaluation;

import java.util.ArrayList;

import static Simulator.Parameters.REPLICATION_TIME_INTERVAL;

/**
 * Created by Yahya on 8/23/2016.
 */
public class DynamicSimulation
{
    long time;
    SkipGraphOperations sgo;
    LookupEvaluation mSkipGraphLookupEvaluation;
    LightChain.LookupEvaluation mBlockchainLookupEvaluation;
    //    LookupEvaluation le;
    private static double previousArrivalTime; //Keeps record of previous arrival time for churn statistics only when the topology is loaded

    public DynamicSimulation(SkipGraph.SkipGraphOperations sgo)
    {
        mSkipGraphLookupEvaluation = new LookupEvaluation();
        if(SkipSimParameters.getSimulationType().equalsIgnoreCase(Constants.SimulationType.BLOCKCHAIN))
        {
            mBlockchainLookupEvaluation = new LightChain.LookupEvaluation();
        }
        if (!AlgorithmInvoker.isNameIDAssignmentDynamic())
        {
            throw new IllegalStateException("Dynamic Simulation Error: Cannot perform dynamic simulation with static name id assignment "
                    + "\n change the name id assignment in confix.txt to one of the dynamic algorithms, DPAD is recommended.");
        }
        else
        {
            this.sgo = sgo;
//            le = new  LookupEvaluation();
        }
    }

    public SkipGraphOperations Simulate(boolean generatingTopology, int currentTime, ArrayList<ChurnDBEntery> churnLog)
    {

        //if (system.isLog())
        System.out.println("Current time: " + currentTime + " Topology index " + SkipSimParameters.getCurrentTopologyIndex());
        /*
        If session length and arrival times have already been loaded into the sgo
         */
        if (!generatingTopology)
        {
            loadChurnLog(currentTime, churnLog);
        }

        /*
        No Node has been loaded from database and hence the topology should be generated
        */
        else
        {
            churnLog = generateTopology(currentTime, churnLog);
        }

        /*
        When replication time is -1 it means that the simulation's concern is totally on lookup evaluation under churn
        and hence, we only have the lookup evaluation all over the times. Otherwise, when we have a well-defined non-negative
        replication time, the lookup evaluation is only done for the sake of the availability vectors being updated
        Also, lookup evaluation is disabled on blockchain simulation.
         */
        if (SkipSimParameters.getSimulationType().equalsIgnoreCase(Constants.SimulationType.DYNAMIC)
        && (SkipSimParameters.getReplicationTime() < 0 || currentTime <= SkipSimParameters.getReplicationTime()))
        {
            mSkipGraphLookupEvaluation.randomizedLookupTests(sgo, currentTime, SkipGraph.LookupEvaluation.SEARCH_FOR_NUMERICAL_ID);
        }
        else if(SkipSimParameters.getSimulationType().equalsIgnoreCase(Constants.SimulationType.BLOCKCHAIN))
        {
            mBlockchainLookupEvaluation.randomizedLookupTests(sgo, currentTime, SkipGraph.LookupEvaluation.SEARCH_FOR_NUMERICAL_ID);
        }
        else
        {
            System.out.println("DynamicSimulation.java: No random lookup takes place at time " + currentTime + " because " +
                    "the replication time has been passed.");
        }


        /*
        If Simulator.system reaches the replication time, the dynamic replication algorithm is called given the replication time
         */
        if(currentTime >= SkipSimParameters.getReplicationTime())
        {
            for (int dataOwner = 0; dataOwner < SkipSimParameters.getDataOwnerNumber(); dataOwner++)
            {
                if (currentTime == SkipSimParameters.getReplicationTime() + REPLICATION_TIME_INTERVAL * dataOwner)
                {
                    System.out.println("Replication for data owner number " + (dataOwner + 1) + " started");
                    long startTime = System.currentTimeMillis();
                    new Simulator.AlgorithmInvoker().dynamicReplication(dataOwner, SkipSimParameters.getReplicationDegree(), sgo.getTG().mNodeSet);
                    long stopTime = System.currentTimeMillis();
                    long elapsedTime = stopTime - startTime;
                    time += elapsedTime;
                    System.out.println("Replication for data owner number " + (dataOwner + 1) + " finished");
                }
            }
        }

        /*
        Replication time of the last dataowner
         */
        int lastReplicationTime;
        /*
        If we are in the dynamic simulation mode, and not the blockchain
         */
        if(SkipSimParameters.getSimulationType().equalsIgnoreCase(Constants.SimulationType.DYNAMIC)
                /*
                And if replication time is greater than zero, meaning that we are simulating for replciation, and not for example churn stabilization
                 */
                && SkipSimParameters.getReplicationTime() > 0)
        {
            /*
            The time in which the last data owner replicates
             */
            lastReplicationTime = SkipSimParameters.getReplicationTime() + (REPLICATION_TIME_INTERVAL * (SkipSimParameters.getDataOwnerNumber() - 1));


            /*
            The immediate subsequent timeslot after the last data owner replicates, the evaluation starts
             */
            if (SkipSimParameters.getReplicationTime() != -1 && currentTime > lastReplicationTime)
            {
                //System.out.println("Replication evaluation has been started");
                repEvaluation.numberOfOnlineReplicasEvaluation(currentTime, sgo, SkipSimParameters.getReplicationAlgorithm());
                //System.out.println("Replication evaluation has been done");
            }
        }

        /*
        Blockchain
         */
        if(SkipSimParameters.getSimulationType().equalsIgnoreCase(Constants.SimulationType.BLOCKCHAIN))
        {
            for(int i = 0; i < SkipSimParameters.getSystemCapacity() ; i++)
            {
                if(((Node) sgo.getTG().mNodeSet.getNode(i)).isOnline())
                {
                    Transaction tx = new Transaction(0, i);
                    sgo.addTXBtoLedger(tx, currentTime, true);
                    //System.out.println("Node " + i + "added a new transaction");
                }
            }
        }

        /*
        Perform the departure of the Nodes that their session length have been terminated at
        the end of the previous time slot.
         */
        sgo.getTG().departureUpdate(currentTime, sgo.getTransactions());

        /*
        Update the average number of online Nodes at the current time slot
         */
        ChurnStochastics.updateTotalAverageOfOnlineNodes(sgo.getTG().mNodeSet.getNumberOfOnlineNodes());

        //System.out.println("Average number of active SkipGraph.Node is started");
        //TODO update the following function based on the new implementation
        //System.out.println("Average number of SkipGraph.Node has been updated");

                    /*
                    Updating the availability vectors of SkipGraph.Nodes
                     */
        //sgo.getTG().mNodeSet.updateAvailabilityVectors();

        /*
        Whatever needs to be done at the end of the last time slot of ALL TOPOLOGIES
         should be placed within the following if statement body
         */
        if (currentTime == SkipSimParameters.getLifeTime() - 1)
        {
            ChurnStochastics.flush();
            mSkipGraphLookupEvaluation.flush();
            if(SkipSimParameters.getSimulationType().equalsIgnoreCase(Constants.SimulationType.BLOCKCHAIN))
            {
                mBlockchainLookupEvaluation.flush();
            }
        }

        /*
        Whatever needs to be done at the end of the last time slot of the LAST TOPOLOGIES
         should be placed within the following if statement body
         */
        if (currentTime == SkipSimParameters.getLifeTime() - 1 && SkipSimParameters.getCurrentTopologyIndex() == SkipSimParameters.getTopologyNumbers())
        {
            if(SkipSimParameters.isLog())
            {
                sgo.getTG().printGeneratorStochastics();
            }
            ChurnStochastics.printChurnStochastics();
            //repEvaluation.finalizingThisTopologyEvaluation();
            //System.out.println("Average number of SkipGraph.Node has been updated");
            //System.out.println("Last SkipGraph.Node came on " + Simulator.system.getLastArrivalTime());
        }

        return sgo;
    }

    /**
     * Loads the churn log (arrival/departures) into the SkipGraphOperation instance
     *
     * @param currentTime current time of the simulation
     * @param churnLog    the churn log that is associated with the current time
     */
    private void loadChurnLog(int currentTime, ArrayList<ChurnDBEntery> churnLog)
    {
        if (currentTime == 0)
        {
            previousArrivalTime = 0;
        }
        for (ChurnDBEntery entery : churnLog)
        {
            int arrivalNodeIndex = entery.getNodeIndex();
            Node arrivingNode = (Node) sgo.getTG().mNodeSet.getNode(arrivalNodeIndex);
            arrivingNode.setOnline();
            arrivingNode.setSessionLength(entery.getSessionLength(), currentTime);
            ChurnStochastics.updateTotalAverageSessionLength(arrivingNode.getSessionLength());
            arrivingNode.setNameID(AlgorithmInvoker.dynamicNameIDAssignment(arrivingNode, sgo, arrivalNodeIndex));
            sgo.insert(arrivingNode, sgo.getTG().mNodeSet, arrivalNodeIndex, AlgorithmInvoker.isNameIDAssignmentDynamic(), currentTime);
            if (SkipSimParameters.getChurnType().equalsIgnoreCase(Constants.Churn.Type.ADVERSARIAL)
                    && SkipSimParameters.getChurnStabilizationAlgorithm().equalsIgnoreCase(Constants.Churn.ChurnStabilizationAlgorithm.INTERLLACED))
            {
                Interlace interlace = new Interlace();
                interlace.rebalanceBucket(arrivingNode.getIndex(), sgo.getTG().mNodeSet);
                interlace.pullBucket(arrivingNode.getIndex(), sgo.getTG().mNodeSet);
            }

            /*
            print the arrival info of the arriving Node if the simulation is enabled with isLog
             */
            printArrivalInfo(arrivingNode, currentTime);

            ChurnStochastics.updateTotalAverageInterArrivalTime(entery.getArrivalTime() - previousArrivalTime);
            previousArrivalTime = entery.getArrivalTime();
        }
        System.out.println("Total number of arrivals: " + ChurnStochastics.getTopologyArrivals());
    }

    private ArrayList<ChurnDBEntery> generateTopology(int currentTime, ArrayList<ChurnDBEntery> churnLog)
    {
        if (currentTime == 0)
        {
            previousArrivalTime = 0;
            System.out.println("DynamicSimulation.java: Generating the topology");
                    /*
                    Generating landmarks
                    */
            sgo.getTG().mLandmarks.generatingLandmarks();
                    /*
                    Generating Nodes
                     */
            sgo.getTG().mNodeSet.generateNodes(true, false, sgo, currentTime, false);
        }

        /*
        If there exists any arrival at this time
         */
        int arrivalNodeIndex = sgo.getTG().randomlyPickOffline();
        Node arrivingNode = (Node) sgo.getTG().mNodeSet.getNode(arrivalNodeIndex);
        while (sgo.getTG().getNextArrivalTime() >= currentTime && sgo.getTG().getNextArrivalTime() < currentTime + 1)
        {
            if (sgo.getTG().mNodeSet.getNumberOfOfflineNodes() >= 0.01 * SkipSimParameters.getSystemCapacity()) //arrival only if we have enough offline Nodes
            {
                /*
                An arrival of a Node
                 */
                arrivingNode.setOnline();
                arrivingNode.setSessionLength(sgo.getTG().generateSessionLength(), currentTime);
                ChurnStochastics.updateTotalAverageSessionLength(arrivingNode.getSessionLength());
                arrivingNode.setNameID(AlgorithmInvoker.dynamicNameIDAssignment(arrivingNode, sgo, arrivalNodeIndex));
                sgo.insert(arrivingNode, sgo.getTG().mNodeSet, arrivalNodeIndex, AlgorithmInvoker.isNameIDAssignmentDynamic(), currentTime);
                churnLog.add(new ChurnDBEntery(arrivalNodeIndex, sgo.getTG().getNextArrivalTime(), arrivingNode.getSessionLength()));

                /*
                print the arrival info of the arriving Node if the simulation is enabled with isLog
                 */
                printArrivalInfo(arrivingNode, currentTime);
                ChurnStochastics.updateTotalAverageInterArrivalTime(currentTime - previousArrivalTime);
                previousArrivalTime = currentTime;
            }
            arrivalNodeIndex = sgo.getTG().randomlyPickOffline();
            arrivingNode = (Node) sgo.getTG().mNodeSet.getNode(arrivalNodeIndex);
            sgo.getTG().updateNextArrivalTime(arrivingNode);
        }

        //System.out.println("Departure update started");
        //sgo.getTG().departureUpdate(currentTime);
        //System.out.println("Departure update finished");

        System.out.println("Total number of arrivals: " + ChurnStochastics.getTopologyArrivals());
        return churnLog;
    }

    private void printArrivalInfo(Node arrivingNode, int currentTime)
    {
        if (SkipSimParameters.isLog())
        {
            arrivingNode.printAvailabilityInfo(currentTime, Constants.Churn.ARRIVAL);
            System.out.println("lookup table");
            arrivingNode.printLookup();
            System.out.println("--------------------------------");
        }
    }
}


