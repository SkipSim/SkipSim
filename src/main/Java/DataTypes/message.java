package DataTypes;

import LightChain.Transaction;
import Simulator.AlgorithmInvoker;
import Simulator.SkipSimParameters;
import SkipGraph.*;
import ChurnStabilization.*;

import java.util.ArrayList;

/**
 * Created by Yahya on 8/28/2016.
 */
public class message
{
    private static ArrayList<Integer> nodeIndices;

    /**
     * Numerical ID distance to the search target that is used to evaluate the closeness to the target
     */
    private static int numIDistanceToTarget;

    /**
     * Name ID similarity to the search target that is used to evaluate the closeness to the target
     */
    private static int nameIDSimilarityToTarget;

    public static int getNumIDistanceToTarget()
    {
        return numIDistanceToTarget;
    }

    public static void setNumIDistanceToTarget(int numIDistanceToTarget)
    {
        message.numIDistanceToTarget = numIDistanceToTarget;
    }

    public static int getNameIDSimilarityToTarget()
    {
        return nameIDSimilarityToTarget;
    }

    public static void setNameIDSimilarityToTarget(int nameIDSimilarityToTarget)
    {
        message.nameIDSimilarityToTarget = nameIDSimilarityToTarget;
    }

    public message()
    {
        //System.out.println("Message.java: a new search message created");
        numIDistanceToTarget = Integer.MAX_VALUE;
        nameIDSimilarityToTarget = Integer.MIN_VALUE;
        nodeIndices = new ArrayList<>();
    }

    public int getSearchPathSize()
    {
        return nodeIndices.size();
    }

    public ArrayList<Integer> getPiggyBackedNodes()
    {
        return nodeIndices;
    }


    //TODO this function only piggybacks the SkipGraph Nodes (i.e., peers) and not blockchain's blocks and transactions
    public void piggyback(int index, Nodes ns, int currentTime)
    {
        ChurnStabilization alg = AlgorithmInvoker.churnStabilization();

        /*
        Online probabilities for churn stabilization
        */
        if(alg != null)
        {
            for(int i: nodeIndices)
            {
                Node n = (Node) ns.getNode(i);
                //sgo.getTG().mNodeSet.getNode(index).addToBucket(n.getNumID(), n.getIndex(), sgo.getTG().mNodeSet.commonPrefixLength(i, index), currentTime);
                alg.insertIntoBucket(n.getIndex(), n.getNumID(), n.getAvailabilityProbability(), index, ns);
            }
        }


        /*
        Availability vectors for replication
         */
        if(SkipSimParameters.isDynamicReplication())
        {
            for(int i: nodeIndices)
            {
                Node n = (Node) ns.getNode(i);
                ((Node) ns.getNode(index)).setAvailabilityTable(n.getIndex(), n.getAvailabilityVector());
            }
        }

        /*
        Adding the intermediate Node information
         */
        if(!nodeIndices.contains(index))
        {
            nodeIndices.add(index);
        }
    }

//    public void piggybackBlock(int index, BlockGraphOperations bgo, int currentTime)
//    {
//        /*ChurnStabilization alg = new AlgorithmInvoker().churnStabilization();
//        if(alg != null)
//        {
//            for(int i: nodeIndices)
//            {
//                Node n = sgo.getTG().mNodeSet.getNode(i);
//                //sgo.getTG().mNodeSet.getNode(index).addToBucket(n.getNumID(), n.getIndex(), sgo.getTG().mNodeSet.commonPrefixLength(i, index), currentTime);
//                alg.insertIntoBucket(n.getIndex(), n.getNumID(), index, sgo.getTG().mNodeSet);
//            }
//        }*/
//        if(!nodeIndices.contains(index))
//        {
//            nodeIndices.add(index);
//        }
//    }

//    /**
//     *
//     * @param searchTarget search target
//     * @param tg TopologyGenerator
//     * @return the closest num ID less than the search target, or -1 if there isn't such
//     */
//    public int getBest(int searchTarget, TopologyGenerator tg)
//    {
//        int minDistance = Integer.MAX_VALUE;
//        int bestIndex = -1;
//        for(int index : nodeIndices)
//        {
//            int numID = tg.mNodeSet.getNode(index).getNumID();
//            if(numID == searchTarget)
//                return index;
//            int distance = Math.abs(searchTarget - numID);
//            if(distance < minDistance && tg.mNodeSet.getNode(index).isOnline())
//            {
//                minDistance = distance;
//                bestIndex = index;
//            }
//        }
//        return bestIndex;
//    }

    public boolean contains(int index)
    {
        return nodeIndices.contains(index);
    }

    public static ArrayList<Integer> getNodeIndices()
    {
        return nodeIndices;
    }

    public void printSearchPath(SkipGraphNodes nodeSet, boolean lookupPrinted)
    {
        System.out.println("----------------------------------");
        System.out.println("message.java: print search path");
        for(int index: nodeIndices)
        {
            if(nodeSet instanceof Nodes)
            {
                System.out.println("Message.java/ Node: index " + index + " numID " + nodeSet.getNode(index).getNumID());
            }
            else
            {
                System.out.println("Message.java/ Block or Transaction: index " + index
                        + " numID " + nodeSet.getNode(index).getNumID()
                        + " owner " + ((Transaction) nodeSet.getNode(index)).getOwnerIndex());
            }
            if(lookupPrinted)
            {
                nodeSet.printLookupNumID(index);
            }

        }
        System.out.println("----------------------------------");
    }
}
