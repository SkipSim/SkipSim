package SkipGraph;

import Simulator.SkipSimParameters;
import Simulator.AlgorithmInvoker;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

public class Nodes extends SkipGraphNodes
{
    //public static int nodeIndex = 0; //number of the SkipGraph.Nodes
    private Node[] mNodeSet;
    //private double[] getNextArrivalTime = new double[system.getNumIDSeed()];
    //private double[] departureTime = new double[system.getNumIDSeed()];
    //TODO: CHANGED TO PROTECTED.
    protected int totalTime = 0;



    /*
    Number of generated lookups
     */


    //TODO for dynamic replication
    //private double averageNumberofRepCandidates = 0;

    //TODO: Visibility is changed to protected. It was previously private
    protected ArrayList<Integer> searchPathLatency;


    private static Random numIDRandomGen;

    public Nodes()
    {
        numIDRandomGen = new Random();
        mNodeSet = new Node[SkipSimParameters.getSystemCapacity()];
        for (int i = 0; i < SkipSimParameters.getSystemCapacity(); i++)
        {
            mNodeSet[i] = new Node(true, i);
        }
        searchPathLatency = new ArrayList<Integer>();
    }

    public ArrayList<Integer> getSearchPathLatency()
    {
        return searchPathLatency;
    }

//    public void updateAvailabilityVectors(int currentTime)
//    {
//        for (int i = 0; i < system.getSystemCapacity(); i++)
//        {
//            mNodeSet[i].updateAvailabilityVector(currentTime);
//        }
//    }
//
//    private void printAvailabilityTable()
//    {
//        for (int i = 0; i < system.getSystemCapacity(); i++)
//        {
//            double sum = 0;
//            for (int t = 0; t < system.getTimeSlot(); t++)
//                sum = sum + mNodeSet[i].getAvailabilityVector(t);
//            if (sum > 0)
//            {
//                for (int t = 0; t < system.getTimeSlot(); t++)
//                    System.out.print(mNodeSet[i].getAvailabilityVector(t) + " ");
//                System.out.println();
//            }
//        }
//    }

    @Override
    public SkipGraphNode getNode(int i)
    {
        return mNodeSet[i];
    }

//  public  double getAverageNumberOfRepCandidates()
//  {
//	  double sum = 0;
//	  int  counter = 0;
//	  for(int i = 0 ; i < Simulator.system.getSystemCapacity(); i++)
//	  {
//		  if(!mNodeSet[i].isEmpty(this))
//		  {
//			  sum += numberOfPossibleReplicationCandidates(i);
//			  counter++;
//		  }
//	  }
//	  averageNumberofRepCandidates += (double)(sum / counter);
//	  return sum;
//  }

//  public int numberOfPossibleReplicationCandidates(int numID)
//  {
//	   int number = 0;
//	   for(int i = 0 ; i < Simulator.system.getNumIDSeed() ; i++)
//		{
//			double sum = 0;
//			if(isNumIDAssigned(i))
//				for(int t = 0 ; t < repTools.getTimeSlots() ; t++)
//					sum = sum + DataTypes.nodesTimeTable.getLocalAvailability(numID, i, t);
//			if(sum > 0)
//			{
//				number++;
//			}
//		}
//	   
//	   return number;
//  }

    public void clearnodes()
    {
        for (int i = 0; i < SkipSimParameters.getSystemCapacity(); i++)
        {
            setNode(i, null);
        }
    }

    @Override
    public void setNode(int index, SkipGraphNode skipGraphNode)
    {
        mNodeSet[index] = (Node) skipGraphNode;
    }

    public int nodeLength()
    {
        return mNodeSet.length;
    }


    public void renewReplicationInfo()
    {
        for (int i = 0; i < SkipSimParameters.getSystemCapacity(); i++)
        {
            mNodeSet[i].clearReplicaIDSet();
            mNodeSet[i].clearDataRequesterIDSet();
        }
    }

    /*
  Renews a SkipGraph.Node after reading from the skipsim file
  Before renew takes place, SkipGraph.Node only has a coodination
   */
    public void renewNode(int index)
    {
        /*
        replication variables
		 */
        mNodeSet[index].clearDataRequesterIDSet();
        mNodeSet[index].clearReplicaIDSet();
        mNodeSet[index].setCorrespondingReplica(-1);


		/*
        Aggregation.Aggregation parameters
		 */
        mNodeSet[index].setParent(-1);
        mNodeSet[index].resetChildSet();
        mNodeSet[index].resetPathToRoot();


        if (index == 0)
        {
            mNodeSet[index].setNumID(0);
        }
        else
        {
            mNodeSet[index].setNumID(getRandomNumID(index));
        }

        //Used only for static simulations and hence does not need
        //mNodeSet[index].setOnline();


        for (int i = 0; i < SkipSimParameters.getLookupTableSize(); i++)
            for (int j = 0; j < 2; j++)
            {
                mNodeSet[index].setLookup(i, j, -1);
                //mNodeSet[index].setBackup(i, j, -1);
            }
    }

    /**
     * @param nodeIndex the Node index from mNodeSet
     * @return zero if index if zero, otherwise a random non zero num ID
     */
    public static int getRandomNumID(int nodeIndex)
    {
        /*
         */
        if (nodeIndex == 0)
        {
            return 0;
        }
        else
        {
            int numID = (int) Math.abs(numIDRandomGen.nextInt(10 * SkipSimParameters.getSystemCapacity())) + 1;
            return numID;
        }

    }

    public int getTotalTime()
    {
        return totalTime;
    }

    public void setTotalTime(int i)
    {
        totalTime = i;
    }

    public void addTime(int destination, int source)
    {
        if (destination != -1 && source != -1)
        {
            double latency = mNodeSet[source].getCoordinate().distance(mNodeSet[destination].getCoordinate());
            searchPathLatency.add((int) latency);
            totalTime += latency;

        }
    }

    public void addMaxTime(int destination1, int destination2, int source)
    {
        if (source != -1)
        {
            if (destination1 != -1 && destination2 != -1)
            {
                totalTime += Math.max(mNodeSet[source].getCoordinate().distance(mNodeSet[destination1].getCoordinate()), mNodeSet[source].getCoordinate().distance(mNodeSet[destination2].getCoordinate()));
            }
            else if (destination1 != -1)
            {
                totalTime += mNodeSet[source].getCoordinate().distance(mNodeSet[destination1].getCoordinate());
            }
            else if (destination2 != -1)
            {
                totalTime += mNodeSet[source].getCoordinate().distance(mNodeSet[destination2].getCoordinate());
            }
        }

    }

    public void resetTotalTime()
    {

        //System.out.println(searchPathLatency.toString() + " " + totalTime);
        totalTime = 0;
        searchPathLatency = new ArrayList<>();
    }


    public int commonBits(int i, int j)
    {
        String s1 = mNodeSet[i].nameID;
        String s2 = mNodeSet[j].nameID;

        int k = 0;

        if (s1.length() > 0 && s2.length() > 0)
        {
            while (s1.charAt(k) == s2.charAt(k))
            {
                k++;
                if (k >= s1.length() || k >= s2.length())// || k >= Simulator.system.nameIDsize )
                {
                    break;
                }
            }
        }

        return k;
    }

    public int minNameIDSize(int i, int j)
    {
        String s1 = mNodeSet[i].nameID;
        String s2 = mNodeSet[j].nameID;

        if (s1.length() < s2.length())
        {
            return s1.length();
        }

        else
        {
            return s2.length();
        }
    }

//    public int minBatteryStatus()
//    {
//        int min = Integer.MAX_VALUE;
//        int index = 0;
//        for (int i = 0; i < system.getSystemCapacity(); i++)
//        {
//            if (mNodeSet[i].batteryLevel < min)
//            {
//                min = (int) mNodeSet[i].batteryLevel;
//                index = i;
//            }
//
//        }
//
//        return (int) mNodeSet[index].batteryLevel;
//    }

//    public int networkBatteryAverage()
//    {
//        double sum = 0;
//        for (int i = 0; i < system.getSystemCapacity(); i++)
//            sum += mNodeSet[i].batteryLevel;
//        sum = sum / system.getSystemCapacity();
//
//        return (int) sum;
//
//    }

//    public void energyReset()
//    {
//        for (int i = 0; i < system.getSystemCapacity(); i++)
//        {
//            mNodeSet[i].energyReset();
//        }
//    }

//    public boolean networkShutdownCheck()
//    {
//        for (int i = 0; i < system.getSystemCapacity(); i++)
//        {
//            if (!mNodeSet[i].isDeactive())
//            {
//                return false;
//            }
//
//        }
//        return true;
//    }

    public int ClosestLandmark(int nodeIndex, Landmarks L)
    {
        Node n = mNodeSet[nodeIndex];
        double min = Double.MAX_VALUE;
        int index = 0;
        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            if (n.getCoordinate().distance(L.getLandmarkCoordination(i)) < min)
            {
                min = n.getCoordinate().distance(L.getLandmarkCoordination(i));
                index = i;
            }
        }

        return index;
    }

    public void updateClosestLandmark(Landmarks L)
    {
        for (int i = 0; i < SkipSimParameters.getSystemCapacity(); i++)
        {
            mNodeSet[i].setClosetLandmarkIndex(ClosestLandmark(i, L));
        }
    }

    public int offlineNodesCounter()
    {
        int num = 0;
        for (int i = 0; i < SkipSimParameters.getSystemCapacity(); i++)
        {
            if (mNodeSet[i].isOffline())
            {
                num++;
            }
        }

        return num;
    }

    public boolean nameIDsDoubleCheck()
    {
        boolean flag = true;
        for (int i = 0; i < SkipSimParameters.getSystemCapacity(); i++)
            for (int j = 0; j < SkipSimParameters.getSystemCapacity(); j++)
            {
                if (i == j)
                {
                    continue;
                }
                else if (mNodeSet[i].nameID.equals(mNodeSet[j].nameID) && !mNodeSet[i].nameID.isEmpty())
                {
                    System.out.println("Same name id: " + i + " " + j + "\n" + mNodeSet[i].nameID + " " + mNodeSet[j].nameID);
                    flag = false;
                }
            }
        if (flag)
        {
            System.out.println("No match was found!");
        }
        return true;
    }


    public int getNumberOfOnlineNodes()
    {
        int counter = 0;
        for (int i = 0; i < SkipSimParameters.getSystemCapacity(); i++)
        {
            if (mNodeSet[i].isOnline())
            {
                //topologyTotalOnlineNodes++;
                counter++;
            }
        }
        return counter;
    }

    public ArrayList<Integer> getIndicesOfOnlineNodes()
    {
        ArrayList<Integer> onlineNodes = new ArrayList<>();
        int counter = 0;
        for (int i = 0; i < SkipSimParameters.getSystemCapacity(); i++)
        {
            if (mNodeSet[i].isOnline())
            {
                //topologyTotalOnlineNodes++;
                onlineNodes.add(i);
            }
        }
        return onlineNodes;
    }



    public int getNumberOfOfflineNodes()
    {
        int counter = 0;
        for (int i = 0; i < SkipSimParameters.getSystemCapacity(); i++)
        {
            if (mNodeSet[i].isOffline())
            {
                //topologyTotalOnlineNodes++;
                counter++;
            }
        }
        //System.out.println("Nodes.java: number of offline Nodes" + counter);
        return counter;
    }

//TODO awake
//    public double correlation(int id1, int id2)
//    {
//        double sum = 0;
//        for (int t = 0; t < system.getTimeSlot(); t++)
//        {
//            sum = sum + (mNodeSet[id1].getAvailabilityVector(t) * mNodeSet[id1].getAvailabilityVector(t));
//        }
//
//        return Math.sqrt(sum);
//    }


    /**
     * @return average number of times a Node received update from other Nodes
     */
    public double averageNumberOfReceivedUpdates()
    {
        double average = 0;
        for (int i = 0; i < SkipSimParameters.getSystemCapacity(); i++)
        {
            double average_row = 0;
            for (int j = 0; j < SkipSimParameters.getSystemCapacity(); j++)
            {
                average_row += mNodeSet[i].getLocalNumberOfUpdatesTable(j);
            }
            average += (average_row / SkipSimParameters.getSystemCapacity());
        }

        return (average / SkipSimParameters.getSystemCapacity());
    }


    private void print(String message)
    {
        if (SkipSimParameters.isLog())
        {
            System.out.println("Nodes: " + message);
        }
    }


    /**
     * @param shouldNumIDBeAssigned
     * @param shouldBeInserted
     * @param sgo
     */
    public void generateNodes(boolean shouldNumIDBeAssigned, boolean shouldBeInserted, SkipGraphOperations sgo, int currentTime, boolean isTest)
    {
        for (int i = 0; i < SkipSimParameters.getSystemCapacity(); i++)
        {
            Node n = new Node(shouldNumIDBeAssigned, i);
            Point p = new Point();
            if(!isTest)
            {
                if (SkipSimParameters.getNodeGenerationStrategy().equals("landmark"))
                {
                    p = sgo.getTG().LandmarkBasedSeedRandomNodeGenerator();
                }
                else
                {
                    p = sgo.getTG().UniformRandomNodeGenerator();
                }
            }
            else
            {
                //We are in test mode, and hence the mCoordinate is generated randomly
                p.x = Math.abs(numIDRandomGen.nextInt(SkipSimParameters.getDomainSize() - 1));
                p.y = Math.abs(numIDRandomGen.nextInt(SkipSimParameters.getDomainSize() - 1));
            }

            n.getCoordinate().x = p.x;
            n.getCoordinate().y = p.y;

            if (shouldBeInserted)
            {
                if (AlgorithmInvoker.isNameIDAssignmentDynamic())
                {
                    n.nameID = AlgorithmInvoker.dynamicNameIDAssignment(n, sgo, i);
                }
                sgo.insert(n, sgo.getTG().mNodeSet, i, AlgorithmInvoker.isNameIDAssignmentDynamic(), currentTime);
            }
            else
            {
                mNodeSet[i] = n;
            }

            System.out.println("Nodes.java: A Node generated: Node name id is " + n.nameID + " numerical id is " + n.getNumID() + " Simulator.system index =  " + i);

        }

    }


    public void printLookupOnlineStatus(int index)
    {
        for (int i = SkipSimParameters.getLookupTableSize() - 1; i >= 0; i--)
        {
            boolean right = false;
            boolean left  = false;
            if(mNodeSet[index].getLookup(i, 0) != -1)
                left = mNodeSet[mNodeSet[index].getLookup(i, 0)].isOnline();
            if(mNodeSet[index].getLookup(i, 1) != -1)
                right = mNodeSet[mNodeSet[index].getLookup(i, 1)].isOnline();

            System.out.println("Level: " + i + "   Left: " + left + "   Right: " + right);
        }

    }

}
