package LightChain;

import SkipGraph.SkipGraphNode;
import SkipGraph.Node;
import SkipGraph.Nodes;

import java.util.HashSet;
import java.util.Random;

import static java.util.Objects.hash;


public class Transaction extends SkipGraphNode
{
    /*
    We consider the hashed value of the transactions and blocks in 32 bits java Object hashed values.
     */
    public static int NAME_ID_SIZE = 32;
    public static int LOOKUP_TABLE_SIZE = 32;

    private static Random sRandom = new Random();


    /**
     * Transaction wire or script i.e., the entire unique content of the transaction
     */
    protected int Wire;

//    /**
//     * Index of the transaction in the Transactions set, which acts as a centralized database for the sake
//     * of saving space.
//     */
//    protected int Index;

    /**
     * Index of the owner of the transaction, it is analogous to the IP address of the owner
     */
    protected int mOwnerIndex;

    public void setOwnerIndex(int ownerIndex)
    {
        mOwnerIndex = ownerIndex;
    }

    /**
     * The pointer to the previous Block on the blockchain, corresponds to the hashed value of the previous block, and not
     * the previous block's index. BE CAREFUL!!
     */
    protected int Previous;

//    /**
//     * Name id of the transaction, binary representation of the previous Block
//     */
//    protected String NameID;

//    /**
//     * Numerical id of the transaction, i.e., hashed value of the transaction
//     */
//    protected int NumID;

    /**
     * Indices set of the signers
     */
    protected HashSet<Integer> SignersSet;

    /**
     * Indices of the replicas of this transaction
     */
    protected HashSet<Integer> ReplicaSet;

    public Transaction(int previous, int owner)
    {
        super();
        this.mOwnerIndex = owner;
        this.Previous = previous;
        this.nameID = generateNameID(previous);

        this.Wire = sRandom.nextInt(Integer.MAX_VALUE);
        SignersSet = new HashSet<>();
        ReplicaSet = new HashSet<>();
        ReplicaSet.add(owner);
        this.numID = hashCode();

        lookup = new int[NAME_ID_SIZE][2];
        for(int i = 0; i < NAME_ID_SIZE; i++)
        {
            for(int j = 0 ; j < 2; j++)
            {
                lookup[i][j] = -1;
            }
        }
    }

    /**
     * Simulates the signing of the transaction, by adding index of the signer to the SignerSet
     * @param signerIndex index of the signer Node
     */
    public void Sign(int signerIndex)
    {
        SignersSet.add(signerIndex);
    }

    /**
     *
     * @param signerIndex index of the signer Node
     * @return True if the Node that is denoted by signerIndex has already signed the Block, and false otherwise
     */
    public boolean VerifySignature(int signerIndex)
    {
        return SignersSet.contains(signerIndex);
    }

    /**
     *
     * @param index index of the transaction inside the Transactions dataset.
     */
    public void setIndex(int index)
    {
        this.index = index;
    }

    /**
     *
     * @param previous hashed value of the previous block
     * @return binary representation of the hashed value of the previous block within BlockNameIDSize bits
     */
    private String generateNameID(int previous)
    {
        String nameID = Integer.toBinaryString(previous);

        while (nameID.length() < NAME_ID_SIZE)
            nameID = "0" + nameID;

        return nameID;
    }

    public int getIndex()
    {
        return index;
    }

    public int getOwnerIndex()
    {
        return mOwnerIndex;
    }

    public int getPrevious()
    {
        return Previous;
    }


    /**
     * Adds address of the replica Node to the set of replicas for this transaction
     * @param replicaIndex the new replica Node's index
     */
    public void addToReplicaSet(int replicaIndex)
    {
        ReplicaSet.add(replicaIndex);
    }

    @Override
    public int hashCode()
    {
        Random random = new Random();
        //TODO should be tested against the result
        //return hash(Previous, Wire, ReplicaSet.hashCode());
        return Math.abs(random.nextInt(100));
    }

    /**
     *
     * @param i a counter on the validator number
     * @return the computed numerical ID of the ith validator
     */
    public int getValidatorNumID(int i)
    {
        //TODO should be tested against the result
        return hash(Previous, Wire, i);
    }

    /**
     *
     * @param ns an instance of the Node set
     * @return number of online replica holders of the transaction
     */
    public int numberOfOnlineReplicaHolders(Nodes ns)
    {
        int counter = 0;
        for(int index: ReplicaSet)
        {
            if(((Node) ns.getNode(index)).isOnline())
            {
                counter++;
            }
        }
        return counter;
    }


    //    public boolean isOnline(Nodes mNodeSet)
//    {
//        if (ownerIndex == -1)
//        {
//            return false;
//        }
//        return mNodeSet.getNode(ownerIndex).isOnline();
//    }

}