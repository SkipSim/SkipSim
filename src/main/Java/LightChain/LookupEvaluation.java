package LightChain;

import ChurnStabilization.ChurnStabilization;
import DataTypes.message;
import Simulator.AlgorithmInvoker;
import Simulator.SkipSimParameters;
import SkipGraph.Node;
import SkipGraph.SkipGraphOperations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class LookupEvaluation extends SkipGraph.LookupEvaluation
{
    public LookupEvaluation()
    {
        super();
    }


    @Override
    public void flush()
    {

    }

    /**
     * This function performs random search for numerical IDS only for a single time slot and returns the success ratio
     * of the search for that time slot. It additionally updates the average search latency, accompained with the average
     * search latency of the successful and unsuccessful searches.
     * @param random the search random generator, use the getter of searchRandomGenerator of the same class
     * @return -1 if could not perform the search due to the low number of online Nodes, otherwise the success search ratio
     */
    @Override
    protected double randomizedSearchForNumericalIDs(SkipGraphOperations skipGraphOperations, Random random, int currentTime)
    {
        double averageSearchTime = 0;
        double averageSuccessfulSearchTime = 0;
        double averageFailedSearchTime = 0;
        int numberOfSuccessfulSearches = 0;
        int numberOfFailedSearches = 0;

        int numOfActiveNodes = skipGraphOperations.getTG().mNodeSet.getNumberOfOnlineNodes();

        if (numOfActiveNodes < 0.01 * SkipSimParameters.getSystemCapacity())
        {
            return -1;
        }

        /*
        Deciding on the number of iterations
         */
        int iterations = 0;
        while (iterations == 0)
            iterations = random.nextInt(numOfActiveNodes * (numOfActiveNodes - 1) / 2) + 1;

        /*

         */
        double counter = 0;
        /*
        Generating the seed to pick the search target and search initiator, it is implemented as an arraylist that
        is initialized to the set of all online Nodes, and then each time a search target or initiator is chosen, it
        is removed from the seed.
         */
        ArrayList<Integer> onlineNodeOriginalList = skipGraphOperations.getTG().mNodeSet.getIndicesOfOnlineNodes();
        ArrayList<Integer> onlineNodesSearchSeed = (ArrayList<Integer>) onlineNodeOriginalList.clone();

        for (int i = 0; i < iterations; i++)
        {
            /*
            If size of seed is less than two elements, it is being initialized again with the set of all the
            online Nodes.
             */
            if (onlineNodesSearchSeed.size() <= 2)
            {
                onlineNodesSearchSeed = (ArrayList<Integer>) onlineNodeOriginalList.clone();
                Collections.shuffle(onlineNodesSearchSeed);
            }

            /*
            Picking and removing search target from the seed
             */
            int searchTargetOwnerIndex = random.nextInt(onlineNodesSearchSeed.size() - 1);
            int searchTargetOwner = onlineNodesSearchSeed.get(searchTargetOwnerIndex);
            int searchTargetTXBIndex = -1;
            while (searchTargetTXBIndex == -1)
            {
                onlineNodesSearchSeed.remove(searchTargetOwnerIndex);
                searchTargetTXBIndex = ((Node) skipGraphOperations.getTG().mNodeSet.getNode(searchTargetOwner)).chooseRandomTXB(random);
            }

            int searchTarget = skipGraphOperations.getTransactions().getNode(searchTargetTXBIndex).getNumID();


            /*
            Picking and removing search target from the seed
             */
            int searchInitiatorIndex = random.nextInt(onlineNodesSearchSeed.size() - 1);
            int searchInitiator = onlineNodesSearchSeed.get(searchInitiatorIndex);
            onlineNodesSearchSeed.remove(searchInitiatorIndex);


            message m = new message();

            Node nodeSearchInitiator = (Node) skipGraphOperations.getTG().mNodeSet.getNode(searchInitiator);

            /*
            Determining the direction of search
             */
            int searchDirection = SkipGraphOperations.LEFT_SEARCH_DIRECTION;
            if(searchTarget >= nodeSearchInitiator.getNumID())
            {
                searchDirection = SkipGraphOperations.RIGHT_SEARCH_DIRECTION;
            }
            int searchResult = skipGraphOperations.SearchByNumID(skipGraphOperations.getTG().mNodeSet.getNode(searchTarget).getNumID(),
                    nodeSearchInitiator,
                    m,
                    SkipSimParameters.getLookupTableSize() - 1,
                    currentTime,
                    skipGraphOperations.getTG().mNodeSet, searchDirection);


            /*
            The following commented code is only for the sake of checking the correctness of search for name ID
             */
//            int nameIDsearchResult = sgo.SearchByNameID(sgo.getTG().mNodeSet.getNode(searchTarget).getNameID(),
//                    nodeSearchInitiator,
//                    sgo.getTG().mNodeSet,
//                    nodeSearchInitiator.getLookup(0, 1),
//                    nodeSearchInitiator.getLookup(0, 0),
//                    0,
//                    new message(),
//                    new ArrayList<Integer>());




            averageSearchTime += skipGraphOperations.getTG().mNodeSet.getTotalTime();

            /*
            The Node that is returned as the search result
             */
            Node nodeSearchResult = (Node) skipGraphOperations.getTG().mNodeSet.getNode(searchResult);
            /*
            The Node corresponding to the search target
             */
            Node nodeSearchTarget = (Node) skipGraphOperations.getTG().mNodeSet.getNode(searchTarget);

/*
The following commented code is only for the sake of checking the correctness of search for name ID
 */
//            if(searchTarget != nameIDsearchResult)
//            //if(!nodeSearchTarget.getNameID().equalsIgnoreCase(sgo.getTG().mNodeSet.getNode(nameIDsearchResult).getNameID()))
//            {
//                System.err.println("Error in correctness of search for name ID result: "
//                        + sgo.getTG().mNodeSet.getNode(nameIDsearchResult).getNameID() + " " + sgo.getTG().mNodeSet.getNode(nameIDsearchResult).getIndex()
//                        + " target: " + nodeSearchTarget.getNameID() + " " + nodeSearchTarget.getIndex());
//            }

            /*
            Checks if the search has been conducted successfully
             */
            if (nodeSearchResult.getNumID() == nodeSearchTarget.getNumID())
            {
                ChurnStabilization alg = AlgorithmInvoker.churnStabilization();
                //System.out.println(m.getSearchPathSize());
                for (int piggyIndex : m.getPiggyBackedNodes())
                {
                    Node n = (Node) skipGraphOperations.getTG().mNodeSet.getNode(piggyIndex);
                    //sgo.getTG().mNodeSet.getNode(index).addToBucket(n.getNumID(), n.getIndex(), sgo.getTG().mNodeSet.commonPrefixLength(i, index), currentTime);
                    if(alg != null)
                        alg.insertIntoBucket(n.getIndex(), n.getNumID(), n.getAvailabilityProbability(), piggyIndex, skipGraphOperations.getTG().mNodeSet);
                }
                counter++;
                averageSuccessfulSearchTime += skipGraphOperations.getTG().mNodeSet.getTotalTime();
                numberOfSuccessfulSearches++;

            }
            else
            {
                averageFailedSearchTime += skipGraphOperations.getTG().mNodeSet.getTotalTime();
                numberOfFailedSearches++;
            }

        }


        if (iterations < 0)
        {
            System.err.println("SkipGraphOperations.java: All pairs random Lookup failed");
            System.exit(0);
        }

        return (double) counter / iterations;
    }


    @Override
    public void randomizedLookupTests(SkipGraphOperations sgo, int currentTime, int searchType)
    {

    }
}
