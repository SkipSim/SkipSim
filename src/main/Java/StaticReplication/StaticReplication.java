package StaticReplication;

import DataTypes.Constants;
import Simulator.SkipSimParameters;

import SkipGraph.Node;
import SkipGraph.SkipGraphOperations;
import net.sf.javailp.SolverFactoryLpSolve;
import net.sf.javailp.*;

import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * Created by Dell on 26.08.2016.
 */
public abstract class StaticReplication
{

    private static double[] ratioDataSet = new double[SkipSimParameters.getTopologyNumbers()];
    private static int[] replicationDegreeDataBase = new int[SkipSimParameters.getTopologyNumbers()];
    public int[][] realDistance;
    public double[][] normalizedNameidDistances;
    public int[][] nameidsDistance;
    public boolean[][] realReplicaAssignment;
    public boolean[][] realWorldReplicaAssignment;
    public boolean[] realWorldReplicaSet;
    protected int subProblemNameIDSize;
    protected int[] regionPopulation;
    protected int[] dataRequesterNumbers;
    protected SkipGraphOperations sgo;
    /**
     * pathHistoragm[i] shows the number of pathes to a data owner that SkipGraph.Node i is located
     */
    protected int[] pathHistogram;
    private int[] repShares;
    /**
     * Number of replicas or the delay bound to achieved, this is mainly a playground value, as readonly value it is suggested
     * to use the Simulator.system.getReplicationDegree()
     */
    // private int MNR;
    // private int NOR;
    //private int M;
    private int[] adaptiveSubproblemSizes; //only for the adaptice SSLP
    //dynamicRealWorldReplicaSet[i][t] == true if and only if the replication algorithm determines SkipGraph.Node i as a replica in the t time slot
    //private static boolean[][]    dynamicRealWorldReplicaSet = new boolean [Simulator.system.numIDSeed][timeSlots];
    private boolean[] dynamicRealWorldReplicaSet = new boolean[SkipSimParameters.getSystemCapacity()];

    public int getAdaptiveSubproblemSizes(int index)
    {
        return adaptiveSubproblemSizes[index];
    }

    public StaticReplication()
    {
        //badCandidates = new ArrayList<>();
        regionPopulation = new int[SkipSimParameters.getLandmarksNum()];
        dataRequesterNumbers = new int[SkipSimParameters.getLandmarksNum()];
        subProblemNameIDSize = SkipSimParameters.getNameIDLength();

        //localReplicaAssignment = new boolean[nameSpace][nameSpace];`
        repShares = new int[SkipSimParameters.getLandmarksNum()];
//                MNR = Simulator.system.getReplicationDegree(); //maximum number of replicas for an object
        //ratioDataSet = new double[Simulator.system.getTopologyNumbers()];
//                NOR = Simulator.system.getDataRequesterNumber();
        //M = (int) Simulator.system.getSystemCapacity() / NOR;
        adaptiveSubproblemSizes = new int[SkipSimParameters.getLandmarksNum()];
        /*
        We initialize sgo with false parameter as we do not assume the blockchain operation in timeless
        static simulation
         */
        sgo = new SkipGraphOperations(false);
        pathHistogram = new int[SkipSimParameters.getSystemCapacity()];
    }

    public abstract double Algorithm(SkipGraphOperations sgo, int dataOwnerID);

    public void setRepShares(int index, int value)
    {
        this.repShares[index] = value;
    }


//        public int getReplicationDegree()
//            {
//                return Simulator.system.getReplicationDegree();
//            }
//
//        public void setReplicationDegree(int mnr)
//            {
//                Simulator.system.setReplicationDegree(mnr);
//            }


//    public int getM()
//        {
//            return M;
//        }

    public int RepOnPath(int num, int startIndex, int RepNum, boolean histogramUpdate, int dataOwnerID)
    {
        int localRepCounter = 0;
        int level = SkipSimParameters.getLookupTableSize() - 1; //Start from the top
        int next = -1;
        int before = startIndex;
        if (sgo.getTG().mNodeSet.getNode(startIndex).getLookup(0, 0) == -1 && sgo.getTG().mNodeSet.getNode(startIndex).getLookup(0, 1) == -1)
        { // if only the introducer exists
            System.out.println("Search by num id for " + num + " resulted in null");
            return RepNum;
        }
        else if (sgo.getTG().mNodeSet.getNode(startIndex).getNumID() < num)
        {

            while (level > 0 && sgo.getTG().mNodeSet.getNode(startIndex).getLookup(level, 1) == -1) level = level - 1;
            while (level > 0)
            {
                if (sgo.getTG().mNodeSet.getNode(startIndex).getLookup(level, 1) != -1)
                {
                    if (sgo.getTG().mNodeSet.getNode(sgo.getTG().mNodeSet.getNode(startIndex).getLookup(level, 1)).getNumID() > num)
                        level = level - 1;
                    else break;
                }
                else level--;
            }

            if (level == 0)
            {
                System.out.println("Search by num id for " + num + " resulted in " + sgo.getTG().mNodeSet.getNode(sgo.getTG().mNodeSet.getNode(startIndex).getLookup(level, 1)).getNumID());
                return RepNum;
            }
            if (level > 0)
            {
                next = sgo.getTG().mNodeSet.getNode(startIndex).getLookup(level, 1);
                //Replicate here
                if (histogramUpdate) pathHistogram[next]++;
                else
                {
                    if (!((Node) sgo.getTG().mNodeSet.getNode(next)).getReplicaIDSet().contains(dataOwnerID))
                    {

                        //************Replication Unit***************************************
                        //if (RepNum <= getReplicationDegree() && localRepCounter < 0.2 * getReplicationDegree())
                        if (RepNum <= SkipSimParameters.getReplicationDegree())
                        {
                            if (isPublicReplication() || next < SkipSimParameters.getDataRequesterNumber())
                            {
                                ((Node) sgo.getTG().mNodeSet.getNode(next)).setReplicaIDSet(dataOwnerID);
                                RepNum++;
                                localRepCounter++;
                            }
                        }
                        else
                        {
                            System.out.println("Rep on path terminates the search");
                            return RepNum;
                        }
                        //realWorldReplicaSet[next] = true;
                        //**************
                    }
                }
                //*********************
                sgo.getTG().mNodeSet.addTime(next, startIndex);
                while (level >= 0)
                {
                    if (sgo.getTG().mNodeSet.getNode(next).getLookup(level, 1) != -1)
                    {
                        if (sgo.getTG().mNodeSet.getNode(sgo.getTG().mNodeSet.getNode(next).getLookup(level, 1)).getNumID() <= num)
                        {
                            before = next;
                            next = sgo.getTG().mNodeSet.getNode(next).getLookup(level, 1);
                            //Replicate here
                            if (histogramUpdate) pathHistogram[next]++;
                            else
                            {
                                if (!((Node) sgo.getTG().mNodeSet.getNode(next)).getReplicaIDSet().contains(dataOwnerID))
                                {

                                    //************Replication Unit***************************************
                                    //if (RepNum <= getReplicationDegree() && localRepCounter < 0.2 * getReplicationDegree())
                                    if (RepNum <= SkipSimParameters.getReplicationDegree())
                                    {
                                        if (isPublicReplication()|| next < SkipSimParameters.getDataRequesterNumber())
                                        {
                                            ((Node) sgo.getTG().mNodeSet.getNode(next)).setReplicaIDSet(dataOwnerID);
                                            RepNum++;
                                            localRepCounter++;
                                        }
                                    }
                                    else
                                    {
                                        System.out.println("Rep on path terminates the search");
                                        return RepNum;
                                    }
                                    //realWorldReplicaSet[next] = true;
                                    //**************
                                }
                            }

                            //**************
                            sgo.getTG().mNodeSet.addTime(before, next);
                            if (sgo.getTG().mNodeSet.getNode(next).getNumID() == num)
                            {
                                System.out.println("Search by num id for " + num + " resulted in " + sgo.getTG().mNodeSet.getNode(next).getNumID());
                                return RepNum;
                            }
                        }
                        else level = level - 1;
                    }
                    else level = level - 1;
                }
            }

            System.out.println("Search by num id for " + num + " resulted in " + sgo.getTG().mNodeSet.getNode(next).getNumID());
            return RepNum;
        }
        else
        {
            next = -1;
            while (level > 0 && sgo.getTG().mNodeSet.getNode(startIndex).getLookup(level, 0) == -1) level = level - 1;
            while (level > 0)
            {
                if (sgo.getTG().mNodeSet.getNode(startIndex).getLookup(level, 0) != -1)
                {
                    if (sgo.getTG().mNodeSet.getNode(sgo.getTG().mNodeSet.getNode(startIndex).getLookup(level, 0)).getNumID() < num)
                        level = level - 1;
                    else break;
                }
                else level = level - 1;
            }

            if (level >= 0)
            {
                before = next;
                next = sgo.getTG().mNodeSet.getNode(startIndex).getLookup(level, 0);
                //Replicate here
                if (histogramUpdate) pathHistogram[next]++;
                else
                {
                    if (!((Node)sgo.getTG().mNodeSet.getNode(next)).getReplicaIDSet().contains(dataOwnerID))
                    {

                        //************Replication Unit***************************************
                        //if (RepNum <= getReplicationDegree() && localRepCounter < 0.2 * getReplicationDegree())
                        if (RepNum <= SkipSimParameters.getReplicationDegree())
                        {
                            if (isPublicReplication() || next < SkipSimParameters.getDataRequesterNumber())
                            {
                                ((Node) sgo.getTG().mNodeSet.getNode(next)).setReplicaIDSet(dataOwnerID);
                                RepNum++;
                                localRepCounter++;
                            }
                        }
                        else
                        {
                            System.out.println("Rep on path terminates the search");
                            return RepNum;
                        }
                        //realWorldReplicaSet[next] = true;
                        //**************
                    }
                }

                //**************
                sgo.getTG().mNodeSet.addTime(before, next);
                while (level >= 0)
                {
                    if (sgo.getTG().mNodeSet.getNode(next).getLookup(level, 0) != -1)
                    {
                        if (sgo.getTG().mNodeSet.getNode(sgo.getTG().mNodeSet.getNode(next).getLookup(level, 0)).getNumID() >= num)
                        {
                            before = next;
                            next = sgo.getTG().mNodeSet.getNode(next).getLookup(level, 0);
                            //Replicate here
                            if (histogramUpdate) pathHistogram[next]++;
                            else
                            {
                                if (!((Node) sgo.getTG().mNodeSet.getNode(next)).getReplicaIDSet().contains(dataOwnerID))
                                {

                                    //************Replication Unit***************************************
                                    //if (RepNum <= getReplicationDegree() && localRepCounter < 0.2 * getReplicationDegree())
                                    if (RepNum <= SkipSimParameters.getReplicationDegree())
                                    {
                                        if (isPublicReplication() || next < SkipSimParameters.getDataRequesterNumber())
                                        {
                                            ((Node) sgo.getTG().mNodeSet.getNode(next)).setReplicaIDSet(dataOwnerID);
                                            RepNum++;
                                            localRepCounter++;
                                        }
                                    }
                                    else
                                    {
                                        System.out.println("Rep on path terminates the search");
                                        return RepNum;
                                    }
                                    //realWorldReplicaSet[next] = true;
                                    //**************
                                }
                            }

                            //**************
                            sgo.getTG().mNodeSet.addTime(before, next);
                            if (sgo.getTG().mNodeSet.getNode(next).getNumID() == num)
                            {
                                System.out.println("Search by num id for " + num + " resulted in " + sgo.getTG().mNodeSet.getNode(next).getNumID());
                                return RepNum;
                            }
                        }
                        else level = level - 1;
                    }
                    else level = level - 1;
                }

            }

            System.out.println("Search by num id for " + num + " resulted in " + sgo.getTG().mNodeSet.getNode(next).getNumID());
            return RepNum;
        }
    }

    public int getRepshare(int i)
    {
        return repShares[i];
    }

//    public void Algorithm(SkipGraph.SkipGraphOperations sgo)
//        {
//
//        }

    public void setRatioDataSet(int i, double ratio)
    {
        ratioDataSet[i] = ratio;
    }

    public boolean getRealWorldReplicaSet(int i)
    {
        return realWorldReplicaSet[i];
    }

//    public void setAdaptiveSubProblemSize(int LandmarkIndex)
//        {
//            subProblemSize = adaptiveSubproblemSizes[LandmarkIndex];
//            subProblemNameIDSize = 1;
//            while (Math.pow(2, subProblemNameIDSize) < subProblemSize) subProblemNameIDSize++;
//
//            //System.out.println("The subProblemSize is set to " + subProblemSize + " subProblemNameID size = " + subProblemNameIDSize + " Current MNR: " + MNR + " current prefix " + sgo.getTG().mLandmarks.getDynamicPrefix(LandmarkIndex));
//        }

    public void reset()
    {
        repShares = new int[SkipSimParameters.getLandmarksNum()];
        //MNR = Simulator.system.getReplicationDegree(); //maximum number of replicas for an object
        adaptiveSubproblemSizes = new int[SkipSimParameters.getLandmarksNum()];
        //ratioDataSet = new double[Simulator.system.simRun];
    }

    public void resetRep()
    {
        for (int i = 0; i < SkipSimParameters.getSystemCapacity(); i++)
        {
            ((Node) sgo.getTG().mNodeSet.getNode(i)).clearReplicaIDSet();
            ((Node) sgo.getTG().mNodeSet.getNode(i)).clearDataRequesterIDSet();
            ((Node) sgo.getTG().mNodeSet.getNode(i)).setCorrespondingReplica(-1);
        }


    }


    protected int commonBits(String s1, String s2)
    {
        int k = 0;
        while (s1.charAt(k) == s2.charAt(k))
        {
            k++;
            if (k >= s1.length() || k >= s2.length()) break;
        }
        return k;
    }

    private int landmarkDistances(String s1, String s2)
    {
        int k = 0;
        while (s1.charAt(k) == s2.charAt(k))
        {
            k++;
            if (k >= s1.length() || k >= s2.length()) break;
        }
        if (s1.length() > s2.length()) return s1.length() - k;
        else return s2.length() - k;
    }

    protected int commonBits(int i, int j)
    {
        String s1 = Integer.toBinaryString(i);
        String s2 = Integer.toBinaryString(j);
        while (s1.length() < SkipSimParameters.getNameIDLength()) s1 = "0" + s1;
        while (s2.length() < SkipSimParameters.getNameIDLength()) s2 = "0" + s2;
        int k = 0;
        while (s1.charAt(k) == s2.charAt(k))
        {
            k++;
            if (k >= s1.length() || k >= s2.length()) break;
        }
        return k;
    }

    /*
    Computes the common bits of SkipGraph.Nodes i and j based on the sub problem size
     */
//    protected int commonBitsSubProblemSize(int i, int j, int virtualSystemSize)
//        {
//            String s1 = Integer.toBinaryString(i);
//            String s2 = Integer.toBinaryString(j);
//            while (s1.length() < virtualSystemSize) s1 = "0" + s1;
//            while (s2.length() < virtualSystemSize) s2 = "0" + s2;
//            int k = 0;
//            while (s1.charAt(k) == s2.charAt(k))
//                {
//                    k++;
//                    if (k >= s1.length() || k >= s2.length()) break;
//                }
//            return k;
//        }

    private int findTheClosest(int k, int landmarkIndex, int dataOwnerID)
    {
        //System.out.println("Find the closest call with k = " + k + " and landmark = " + landmarkIndex );
        String s = Integer.toBinaryString(k);

        while (s.length() < subProblemNameIDSize) s = "0" + s;

        //String b = s;

        s = sgo.getTG().mLandmarks.getDynamicPrefix(landmarkIndex) + s;
        System.out.println("Find the closest call with k = " + k + " and landmark = " + landmarkIndex);
        int maxCommon = 0;
        int maxIndex = 0;
        for (int i = 0; i < SkipSimParameters.getSystemCapacity(); i++)
        {
            if (s.equals(((Node) sgo.getTG().mNodeSet.getNode(i)).getNameID()))
            {
                System.out.println("Closest name id to " + s + " is " + ((Node) sgo.getTG().mNodeSet.getNode(i)).getNameID());
                return i;
            }
            else if (commonBits(s, ((Node) sgo.getTG().mNodeSet.getNode(i)).getNameID()) >= maxCommon && !((Node) sgo.getTG().mNodeSet.getNode(i)).getReplicaIDSet().contains(dataOwnerID))
            {
                maxCommon = commonBits(s, ((Node) sgo.getTG().mNodeSet.getNode(i)).getNameID());
                maxIndex = i;
            }
        }
        System.out.println("Closest name id to " + s + " is " + ((Node) sgo.getTG().mNodeSet.getNode(maxIndex)).getNameID() + " with " + maxCommon + " bits name id common prefix length");
        return maxIndex;
    }


    private int findTheClosest(int k)
    {
        String s = Integer.toBinaryString(k);
        while (s.length() < SkipSimParameters.getNameIDLength()) s = "0" + s;
        int maxCommon = 0;
        int maxIndex = 0;
        for (int i = 0; i < SkipSimParameters.getSystemCapacity(); i++)
        {
            if (s.equals(((Node) sgo.getTG().mNodeSet.getNode(i)).getNameID()))
            {
                return i;
            }
            else if (commonBits(s, ((Node) sgo.getTG().mNodeSet.getNode(i)).getNameID()) > maxCommon)
            {
                maxCommon = commonBits(s, ((Node) sgo.getTG().mNodeSet.getNode(i)).getNameID());
                maxIndex = i;
            }
        }
        return maxIndex;
    }

//TODO ready to detach
//    public void replicaSetGenerator(Result R, String set, int size, int dataOwnerID)
//    {
//        String result = new String();
//        result = R.toString();
//
//        for (int i = 0; i < size; i++)
//        {
//            for (int j = 0; j < size; j++)
//            {
//                String target = "X" + i + "," + j + "=1";
//                if (result.contains(target) && set.contains("Real"))
//                {
//                    sgo.getTG().mNodeSet.getNode(j).setDataRequesterID(dataOwnerID);
//                    sgo.getTG().mNodeSet.getNode(j).setCorrespondingReplica(i);
//                }
//                else if (result.contains(target) && set.contains("Local"))
//                {
//                    sgo.getTG().mNodeSet.getNode(j).setDataRequesterID(dataOwnerID);
//                    sgo.getTG().mNodeSet.getNode(j).setCorrespondingReplica(i);
//                    //System.out.println("SkipGraph.Node " + j + " uses " + i);
//                }
//                else if (!set.contains("Real") && !set.contains("Local"))
//                {
//                    // System.out.println("Error in calling Rep_Alg01_LP: replicaSetGenerator with " + set);
//                    System.exit(0);
//                }
//            }
//        }
//    }

    public int replicaSetGenerator2(Result R, String set, int size, int landmarkIndex, int dataOwnerID)
    {
        if (R == null)
        {
            System.out.println("replica optimizer 2, null result input");
            System.exit(0);
        }
        String result = new String();
        result = R.toString();
        boolean flag = false;
        //System.out.println(result);
        for (int i = 0; i < size; i++)
        {
            String target = "Y" + i + "=1";
            if (result.contains(target) && set.contains("Real")) ((Node) sgo.getTG().mNodeSet.getNode(i)).setReplicaIDSet(dataOwnerID);
                //realReplicaSet[i] = true;

            else if (result.contains(target) && set.contains("Local"))
            {
                int replicaIndex = findTheClosest(landmarkIndex, i, dataOwnerID);
                ((Node) sgo.getTG().mNodeSet.getNode(replicaIndex)).setReplicaIDSet(dataOwnerID);
                //realWorldReplicaSet[replicaIndex] = true;
                flag = true;
            }
            else if (!set.contains("Real") && !set.contains("Local"))
            {
                System.exit(0);
            }

        }

        if (!flag) return -1;

        return 0;
    }

    public double privateAverageDelay()
    {
        double delay = 0;
        for (int i = 0; i < SkipSimParameters.getSystemCapacity(); i++)
        {
            if (i < SkipSimParameters.getDataRequesterNumber())
            {
                delay += ((Node) sgo.getTG().mNodeSet.getNode(i)).getCoordinate().distance(((Node) sgo.getTG().mNodeSet.getNode(((Node) sgo.getTG().mNodeSet.getNode(i)).getCorrespondingReplica())).getCoordinate());
            }
        }

        return delay / SkipSimParameters.getDataRequesterNumber();
    }

    public boolean getDynamicRealWorldReplicaSet(int i)
    {
        //return dynamicRealWorldReplicaSet[i][t];
        return dynamicRealWorldReplicaSet[i];

    }

    public int privateTotalDelay(boolean[][] B)
    {
        int delay = 0;
        for (int j = 0; j < SkipSimParameters.getSystemCapacity(); j++)
            for (int i = 0; i < SkipSimParameters.getSystemCapacity(); i++)
            {
                if (B[i][j])
                {
                    if (j < SkipSimParameters.getDataRequesterNumber())
                    {
                        delay += realDistance[i][j];
                        break;
                    }
                }
            }

        return delay;
    }


    public void replicaAssignmentSetGenerator(int size, int dataOwnerID)
    {
        for (int i = 0; i < size; i++)
        {
            int minDistance = Integer.MAX_VALUE;
            int minReplicaIndex = 0;
            for (int j = 0; j < size; j++)
            {
                if (((Node) sgo.getTG().mNodeSet.getNode(j)).getReplicaIDSet().contains(dataOwnerID)
                        && ((Node) sgo.getTG().mNodeSet.getNode(i)).getCoordinate().distance(((Node) sgo.getTG().mNodeSet.getNode(j)).getCoordinate()) < minDistance)
                {
                    minDistance = (int) ((Node) sgo.getTG().mNodeSet.getNode(i)).getCoordinate().distance(((Node) sgo.getTG().mNodeSet.getNode(j)).getCoordinate());
                    minReplicaIndex = j;
                }
            }
            ((Node) sgo.getTG().mNodeSet.getNode(i)).setDataRequesterID(dataOwnerID);
            ((Node) sgo.getTG().mNodeSet.getNode(i)).setCorrespondingReplica(minReplicaIndex);
        }
    }


//        public Result PrivateOptimizer(int[][] L, int size)
//            {
//                SolverFactory factory = new SolverFactoryLpSolve(); // use lp_solve
//                factory.setParameter(Solver.VERBOSE, 0);
//                factory.setParameter(Solver.TIMEOUT, Integer.MAX_VALUE); // set timeout to 100 seconds
//
//                /**
//                 * Constructing a Problem:
//                 * Minimize: Sigma(i)Sigma(j) LijXij
//                 * Subject to:
//                 * for each i,j Yi>= Xij
//                 * Sigma(i)Xij = 1
//                 * Sigma(j)Xij >= Yi
//                 * Sigma(i)Yi <= MNR
//                 */
//
//                Problem problem = new Problem();
//
//                /**
//                 * Part 1: Minimize: Sigma(i)Sigma(j) LijXij
//                 */
//                Linear linear = new Linear();
//                for (int i = 0; i < size; i++)
//                    for (int j = 0; j < size; j++)
//                        {
//                            if (j < Simulator.system.getDataRequesterNumber())
//                                {
//                                    String var = "X" + i + "," + j;
//                                    linear.add(L[i][j], var);
//                                }
//                        }
//                problem.setObjective(linear, OptType.MIN);
//
//
//                /**
//                 * Part 2: for each i,j Yi>= Xij
//                 */
//                for (int i = 0; i < size; i++)
//                    {
//                        for (int j = 0; j < size; j++)
//                            {
//                                linear = new Linear();
//                                String var = "X" + i + "," + j;
//                                linear.add(1, var);
//                                var = "Y" + i;
//                                linear.add(-1, var);
//                                problem.add(linear, "<=", 0);
//                            }
//
//                    }
//
//                /**
//                 * Part 3: Sigma(i)Xij = 1
//                 */
//                for (int j = 0; j < size; j++)
//                    {
//                        linear = new Linear();
//                        for (int i = 0; i < size; i++)
//                            {
//                                String var = "X" + i + "," + j;
//                                linear.add(1, var);
//                            }
//                        problem.add(linear, "=", 1);
//                    }
//
//
//                /**
//                 * Part 4: Sigma(j)Xij >= Yi
//                 */
//                for (int i = 0; i < size; i++)
//                    {
//                        linear = new Linear();
//                        for (int j = 0; j < size; j++)
//                            {
//                                String var = "X" + i + "," + j;
//                                linear.add(-1, var);
//                            }
//
//                        String var = "Y" + i;
//                        linear.add(1, var);
//                        problem.add(linear, "<=", 0);
//                    }
//
//
//                /**
//                 * Part 5: Sigma(i)Yi = MNR
//                 */
//                linear = new Linear();
//                for (int i = 0; i < size; i++)
//                    {
//                        String var = "Y" + i;
//                        linear.add(1, var);
//                    }
//                problem.add(linear, "=", MNR);
//
//
//                /**
//                 * Part 6: Set the type of Xij and Yi
//                 */
//                for (int i = 0; i < size; i++)
//                    {
//                        for (int j = 0; j < size; j++)
//                            {
//                                String var = "X" + i + "," + j;
//                                problem.setVarType(var, Integer.class);
//                            }
//
//                        String var = "Y" + i;
//                        problem.setVarType(var, Integer.class);
//                    }
//
//
//                /**
//                 * Solving the problem
//                 */
//                Solver solver = factory.get(); // you should use this solver only once for one problem
//                Result result = solver.solve(problem);
//                return (result);
//            }

//    public void replicaSetInit()
//    {
//        for(int i = 0 ; i < problemSize ; i++)
//        {
//            realWorldReplicaSet[i] = false;
//            realReplicaSet[i] = false;
//            for(int j = 0 ; j < problemSize ; j++)
//            {
//                realReplicaAssignment[i][j] = false;
//                realWorldReplicaAssignment[i][j] = false;
//            }
//        }
//        for(int i = 0 ; i < subProblemSize ; i++)
//            for(int j = 0 ; j < subProblemSize ; j++)
//            {
//                localReplicaAssignment[i][j] = false;
//            }
//    }

//    public void replicaSetInit()
//        {
//
//            for (int i = 0; i < subProblemSize; i++)
//                for (int j = 0; j < subProblemSize; j++)
//                    {
//                        localReplicaAssignment[i][j] = false;
//                    }
//        }

    public double publicAverageDelay()
    {
        int delay = 0;
        for (int i = 0; i < SkipSimParameters.getSystemCapacity(); i++)
        //if(sgo.getTG().mNodeSet.getNode(i).isDataRequester())
        {
            delay += ((Node) sgo.getTG().mNodeSet.getNode(i)).getCoordinate().distance(((Node) sgo.getTG().mNodeSet.getNode(((Node) sgo.getTG().mNodeSet.getNode(i)).getCorrespondingReplica())).getCoordinate());
            //delay += realDistance[sgo.getTG().mNodeSet.getNode(i).getCorrespondingReplica()][i];
        }

        return delay / SkipSimParameters.getSystemCapacity();
    }

    /*
    Just computes the total delay of region zero
     */
    public double regionZeroAverageDelay()
    {
        sgo.getTG().mNodeSet.updateClosestLandmark(sgo.getTG().mLandmarks);
        int counter = 0;
        double delay = 0;
        for (int i = 0; i < SkipSimParameters.getSystemCapacity(); i++)
            if (((Node) sgo.getTG().mNodeSet.getNode(i)).getClosetLandmarkIndex(sgo.getTG().mLandmarks) == 0)
            {
                counter++;
                delay += realDistance[((Node) sgo.getTG().mNodeSet.getNode(i)).getCorrespondingReplica()][i];
            }

        return delay / counter;
    }

//    public void localReplicaSetInit()
//        {
//            for (int i = 0; i < subProblemSize; i++)
//                for (int j = 0; j < subProblemSize; j++)
//                    {
//                        localReplicaAssignment[i][j] = false;
//                    }
//        }

    public void evaluation(String algName)
    {
        double averageRatio = 0;
        for (int i = 0; i < SkipSimParameters.getTopologyNumbers(); i++)
        {
            //System.out.println(i+ " "+ ratioDataSet[i]);
            averageRatio += ratioDataSet[i];
        }
        averageRatio = (double) averageRatio / SkipSimParameters.getTopologyNumbers();
        double SD = 0;
        for (int i = 0; i < SkipSimParameters.getTopologyNumbers(); i++)
        {
            SD += Math.pow(ratioDataSet[i] - averageRatio, 2);
        }
        SD = (double) SD / SkipSimParameters.getTopologyNumbers();
        SD = Math.sqrt(SD);
        System.out.println("Replciation Simulation: " + algName + " , MNR = " + SkipSimParameters.getReplicationDegree() + " NOR = " + SkipSimParameters.getDataRequesterNumber());
        System.out.println("Average real delay = " + averageRatio + " with SD =  " + SD);
    }

    /*
    Counts each landmark is the closest landmark of how many others
     */
    protected int[] closestLandmarkCounter()
    {
        int[] counter = new int[SkipSimParameters.getLandmarksNum()];
        Arrays.fill(counter, 0);

        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            if (SkipSimParameters.getReplicationType().equalsIgnoreCase(Constants.Replication.Type.PRIVATE) && dataRequesterNumbers[i] == 0) continue;
            double minDistance = Double.MAX_VALUE;
            int minIndex = 0;
            for (int j = 0; j < SkipSimParameters.getLandmarksNum(); j++)
            {
                if (sgo.getTG().mLandmarks.getLandmarkCoordination(i).distance(sgo.getTG().mLandmarks.getLandmarkCoordination(j)) < minDistance && i != j)
                {
                    minDistance = sgo.getTG().mLandmarks.getLandmarkCoordination(i).distance(sgo.getTG().mLandmarks.getLandmarkCoordination(j));
                    minIndex = j;
                }
            }
            counter[minIndex]++;
        }
        return counter;
    }

    /**
     * @return Two dimensional matrix boolean matrix of map, which map[i][j] = true if and only if landmark j's closest landmark is landmark i
     */
    protected boolean[][] closestLandmarkMap()
    {
        boolean[][] map = new boolean[SkipSimParameters.getLandmarksNum()][SkipSimParameters.getLandmarksNum()];
        for (boolean[] row : map)
            Arrays.fill(row, false);
        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            double minDistance = Double.MAX_VALUE;
            int minIndex = 0;
            for (int j = 0; j < SkipSimParameters.getLandmarksNum(); j++)
            {
                if (sgo.getTG().mLandmarks.getLandmarkCoordination(i).distance(sgo.getTG().mLandmarks.getLandmarkCoordination(j)) < minDistance && i != j)
                {
                    minDistance = sgo.getTG().mLandmarks.getLandmarkCoordination(i).distance(sgo.getTG().mLandmarks.getLandmarkCoordination(j));
                    minIndex = j;
                }
            }
            map[minIndex][i] = true;
        }

//        System.out.println("Map matrix");
//        for(int i = 0 ; i < Simulator.system.getLandmarksNum() ; i++)
//        {
//            for (int j = 0; j < Simulator.system.getLandmarksNum(); j++)
//            {
//                if(map[i][j])
//                    System.out.print(1 + " ");
//                else
//                    System.out.print(0 + " ");
//            }
//            System.out.println();
//        }
        return map;
    }

    /**
     * @return the expected number of SkipGraph.Nodes in each region based on the dynamic prefix of landmarks obtained by DPAD or HDPAD
     */
    protected double[] expectedRegionPopulation()
    {
        dataRequesterPopulation();
        double[] P = new double[SkipSimParameters.getLandmarksNum()];
        Arrays.fill(P, 0);

        int sumOfPrefix = 0;
        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            sumOfPrefix += sgo.getTG().mLandmarks.dynamicPrefixLength(i);
        }

        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            P[i] = sgo.getTG().mLandmarks.dynamicPrefixLength(i) * SkipSimParameters.getSystemCapacity() / sumOfPrefix;
            //System.out.println("Region = " + i + " Expected SkipGraph.Nodes = " + P[i] + " Actual number of SkipGraph.Nodes = " + regionPopulation[i]);
        }


        return P;
    }

    /**
     * @param landmarkIndex index of the landmark
     * @return sum of latencies between landmak index to the rest of landmarks
     */
    protected double totalLatencyToLandmark(int landmarkIndex)
    {
        double sum = 0;
        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            sum += sgo.getTG().mLandmarks.getLandmarkCoordination(landmarkIndex).distance(sgo.getTG().mLandmarks.getLandmarkCoordination(i));
        }
        return sum;
    }

    public void repShareDefining(double w1, double w2, double w3)
    {
        /*
        Landmarks pairwise latency
         */
        //double[] latencyToClosestLandmark = new double[Simulator.system.getLandmarksNum()];
        double maxPairwiseLatency = Double.MIN_VALUE;
        double[] totalLatencyToLandmarks = new double[SkipSimParameters.getLandmarksNum()];
        double[][] D = new double[SkipSimParameters.getLandmarksNum()][SkipSimParameters.getLandmarksNum()];
        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            totalLatencyToLandmarks[i] = 0;
            double minLatency = Double.MAX_VALUE;
            for (int j = 0; j < SkipSimParameters.getLandmarksNum(); j++)
            {
                D[i][j] = sgo.getTG().mLandmarks.getLandmarkCoordination(i).distance(sgo.getTG().mLandmarks.getLandmarkCoordination(j));
                if (D[i][j] < minLatency) minLatency = D[i][j];
                if (D[i][j] > maxPairwiseLatency) maxPairwiseLatency = D[i][j];
//                if(repType == Simulator.system.PRIVATE_REPLICATION)
//                {
//                    totalLatencyToLandmarks[i] = dataRequesterNumbers[i];
//                }
//                else
//                {
                totalLatencyToLandmarks[i] += D[i][j];
//                }

            }
            //latencyToClosestLandmark[i] = minLatency;
        }
        //System.out.println("Total latency to landmarks " + Arrays.toString(totalLatencyToLandmarks));
//        /*
//        Computes the expected number of SkipGraph.Nodes per each region based on the length of landmarks' dynamic prefixes
//         */
//       double[] expectedRegionPop = expectedRegionPopulation();
//       if(repType == Simulator.system.PRIVATE_REPLICATION)
//       {
//           for(int i = 0; i < Simulator.system.getLandmarksNum() ; i++)
//               expectedRegionPop[i] = dataRequesterNumbers[i];
//       }

        /*
        closestCounter for each landmark i keeps the number of landmarks that landmark i covers as their closest landmark
        EXCLUDING landmark i itself.
         */
        int[] closestCounter = closestLandmarkCounter();

        /*
        Computing the map of closest landmarks
         */
        boolean[][] map = closestLandmarkMap();
//        /*
//        Average latency of all SkipGraph.Nodes in the Simulator.system to each landmark which comes directly from the HDPAD or DPAD
//         */
//        double[] averageLatency = averageDistanceOfLandmarks();

        /*
        Holds the total score of each landmark on receiving the sub-replication degree
         */
        double[] landmarksPoints = new double[SkipSimParameters.getLandmarksNum()];
        Arrays.fill(landmarksPoints, 0);

//        for(int i = 0 ; i < Simulator.system.getLandmarksNum() ; i++)
//        {
//           // landmarksPoints[i] = (sgo.getTG().mLandmarks.dynamicPrefixLength(i) + closestCounter[i] ) * totalLatencyToLandmarks[i];
//            landmarksPoints[i] = (sgo.getTG().mLandmarks.dynamicPrefixLength(i) ) * totalLatencyToLandmarks[i];
//        }



        /*
        The ordered set of landmarks based on their points so that the replication degree is distributed among in a
        cyclic manner
         */
        int[] orderdLandmarkIndices = new int[SkipSimParameters.getLandmarksNum()];

        /*
        Fining the landmark with the minimum total latency
         */
        double minTotalLatency = Double.MAX_VALUE;
        double maxTotalLatency = Double.MIN_VALUE;
        int minTotalLatencyIndex = 0;
        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            if (totalLatencyToLandmarks[i] > maxTotalLatency)
            {
                maxTotalLatency = totalLatencyToLandmarks[i];
            }

            if (totalLatencyToLandmarks[i] < minTotalLatency)
            {
                minTotalLatency = totalLatencyToLandmarks[i];
                minTotalLatencyIndex = i;
            }
        }
        /*
        remove that value
         */
        landmarksPoints[minTotalLatencyIndex] = -1;
        orderdLandmarkIndices[0] = minTotalLatencyIndex;
        int totalDynamicPrefix = 0;
        for (int landmark = 0; landmark < SkipSimParameters.getLandmarksNum(); landmark++)
        {
            totalDynamicPrefix += sgo.getTG().mLandmarks.dynamicPrefixLength(landmark);
        }
        for (int landmark = 1; landmark < SkipSimParameters.getLandmarksNum(); landmark++)
        {
            double maxPoint = Double.MIN_VALUE;
            int maxIndex = 0;
            /*
            Finding the closest landmark with negative landmark points (i.e., -1) to each landmark
             */
            for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
            {
                            /*
                            SkipGraph.Landmarks i is already selected so there is not need to reprocess it
                             */
                if (landmarksPoints[i] < 0) continue;

                int totalDynamicPrefixOfCoveringLandmarks = 0;
                double minLatency = Double.MAX_VALUE;
                double totalLatency = 0.00001;
                int coverinRegions = 0;
//                    ArrayList<Integer> landmarkLists = new ArrayList<>();
                for (int j = 0; j < SkipSimParameters.getLandmarksNum(); j++)
                {
                    if (D[i][j] < minLatency && landmarksPoints[j] < 0 && i != j) minLatency = D[i][j];


                    if (map[i][j])
                    {
                        if (isPublicReplication()) coverinRegions++;
                        else coverinRegions += dataRequesterNumbers[j];
                    }
                    //totalDynamicPrefixOfCoveringLandmarks += sgo.getTG().mLandmarks.dynamicPrefixLength(j);
                }
                if (isPublicReplication())
                {
                    //System.out.println("index: " + i + "\n" + "w1: " + w1 + " \n " + "prefix score: " + String.valueOf((double) (sgo.getTG().mLandmarks.dynamicPrefixLength(i) + totalDynamicPrefixOfCoveringLandmarks) / totalDynamicPrefix) + " \n " + "w2: " + w2 + " \n " + "latency score " + minLatency / maxPairwiseLatency);
                    landmarksPoints[i] = (w1 * ((double) sgo.getTG().mLandmarks.dynamicPrefixLength(i)) / totalDynamicPrefix) + (w2 * minLatency / maxPairwiseLatency) + (w3 * (double) coverinRegions / SkipSimParameters.getLandmarksNum());
                    //landmarksPoints[i] = (double) (sgo.getTG().mLandmarks.dynamicPrefixLength(i) + (double) totalDynamicPrefixOfCoveringLandmarks / 4) * minLatency;
                }
                else
                    //landmarksPoints[i] = (((double) dataRequesterNumbers[i] / Simulator.system.getDataRequesterNumber()) + ((double) closestCounter[i] / Simulator.system.getLandmarksNum())) * minLatency;
                    landmarksPoints[i] = (w1 * ((double) dataRequesterNumbers[i] / SkipSimParameters.getDataRequesterNumber())) + (w2 * minLatency / maxPairwiseLatency) + (w3 * (double) coverinRegions / SkipSimParameters.getDataRequesterNumber());
                if (landmarksPoints[i] > maxPoint)
                {
                    maxPoint = landmarksPoints[i];
                    maxIndex = i;
                }
            }
            //System.out.println("The landmark points array " + Arrays.toString(landmarksPoints));
            landmarksPoints[maxIndex] = -1;
            orderdLandmarkIndices[landmark] = maxIndex;
        }
        // System.out.println("The ordered set of landmark" + Arrays.toString(orderdLandmarkIndices));
        for (int i = 0; i < SkipSimParameters.getReplicationDegree(); i++)
        {
            repShares[orderdLandmarkIndices[i % SkipSimParameters.getLandmarksNum()]]++;
        }

        System.out.println();
        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            System.out.print("index = " + i + " repshare = " + repShares[i] + " score = " + (int) landmarksPoints[i] + " closest counter = " + closestCounter[i] + " dynamic prefix = " + sgo.getTG().mLandmarks.dynamicPrefixLength(i) + " total latency to others " + totalLatencyToLandmarks[i]);
            if (!isPublicReplication())
                System.out.print("  number of data requesters " + dataRequesterNumbers[i]);
            System.out.println();
        }
    }

    /*
    Dedicates all replication degree to region 0 and makes other regions empty of repshare
     */
    public void oneRegionRepShare()
    {
        Arrays.fill(repShares, 0);
        repShares[0] = SkipSimParameters.getReplicationDegree();

        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            System.out.println(repShares[i] + " " + i + " " + sgo.getTG().mLandmarks.getDynamicPrefix(i));
        }
    }

    /*
    Computes and returns the average distance of each landmark to SkipGraph.Nodes in the Simulator.system
     */
    protected double[] averageDistanceOfLandmarks()
    {
        double[] averageDistance = new double[SkipSimParameters.getLandmarksNum()];
        Arrays.fill(averageDistance, 0);
        for (int i = 0; i < SkipSimParameters.getSystemCapacity(); i++)
        {
            for (int j = 0; j < SkipSimParameters.getLandmarksNum(); j++)
            {
                averageDistance[j] += ((Node) sgo.getTG().mNodeSet.getNode(i)).getCoordinate().distance(sgo.getTG().mLandmarks.getLandmarkCoordination(j));
            }
        }

        for (int j = 0; j < SkipSimParameters.getLandmarksNum(); j++)
        {
            averageDistance[j] /= SkipSimParameters.getSystemCapacity();
        }

        return averageDistance;
    }

    protected void printLandmarkPairwiseLatency()
    {
        System.out.println("SkipGraph.Landmarks pairwise latencies: ");
        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            for (int j = 0; j < SkipSimParameters.getLandmarksNum(); j++)
            {
                System.out.print((int) sgo.getTG().mLandmarks.getLandmarkCoordination(i).distance(sgo.getTG().mLandmarks.getLandmarkCoordination(j)) + "    ");
            }
            System.out.println();
        }
    }

    /*
       Solves an LP  problem for distributing the replicas among the regions, region population should
        be updated before calling this function
        */
    public void improvedRepShare(int repType)
    {
        SolverFactory factory = new SolverFactoryLpSolve(); // use lp_solve
        factory.setParameter(Solver.VERBOSE, 0);
        factory.setParameter(Solver.TIMEOUT, Integer.MAX_VALUE); // set timeout to 100 seconds


//            int maxLandmarkPrefix = Integer.MIN_VALUE;
//            for(int i = 0 ; i < Simulator.system.getLandmarksNum() ; i++)
//                {
//                    if(sgo.getTG().mLandmarks.getDynamicPrefix(i).length() > maxLandmarkPrefix)
//                        maxLandmarkPrefix = sgo.getTG().mLandmarks.getDynamicPrefix(i).length();
//                }
        double[] aveDistance = averageDistanceOfLandmarks();
        double[] aveDistanceToLandmarks = new double[SkipSimParameters.getLandmarksNum()];
        //double   sumDistance = DoubleStream.of(aveDistance).sum();

        double[][] D = new double[SkipSimParameters.getLandmarksNum()][SkipSimParameters.getLandmarksNum()];
        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            aveDistanceToLandmarks[i] = 0;
            for (int j = 0; j < SkipSimParameters.getLandmarksNum(); j++)
            {
                D[i][j] = sgo.getTG().mLandmarks.getLandmarkCoordination(i).distance(sgo.getTG().mLandmarks.getLandmarkCoordination(j));
                aveDistanceToLandmarks[i] += D[i][j];
                //D[i][j] = landmarkDistances(sgo.getTG().mLandmarks.getDynamicPrefix(i), sgo.getTG().mLandmarks.getDynamicPrefix(j));
                if (!isPublicReplication())
                    D[i][j] = D[i][j] * dataRequesterNumbers[j] / SkipSimParameters.getDataRequesterNumber();
            }
            aveDistanceToLandmarks[i] /= (SkipSimParameters.getLandmarksNum() - 1);
        }
        /**
         * Constructing a Problem:
         * Minimize: Sigma(i)Sigma(j) DijXij
         * Subject to:
         * for each i,j Yi>= Xij
         * Sigma(i)Xij = 1
         * Sigma(i)Yi = MNR
         */

        Problem problem = new Problem();

        /**
         * Part 1: Minimize: Sigma(i)Sigma(j) DijXij + sigma(i) aveDistance(i)*(1 - 0.0032 Y(i))
         */
        Linear linear = new Linear();
//            linear.add(1,"Z");
        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            for (int j = 0; j < SkipSimParameters.getLandmarksNum(); j++)
            {
                String var = "X" + i + "," + j;
                //String var = "Y" + i;
                linear.add(D[i][j], var);
            }
        }
//            for(int i = 0 ; i < Simulator.system.getLandmarksNum() ; i++)
//            {
//                    String var = "Y"+i;
//                    linear.add(aveDistance[i], var);
//            }
        problem.setObjective(linear, OptType.MIN);


        /**
         * Part 2: for each i,j Yi>= Xij
         */
        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            for (int j = 0; j < SkipSimParameters.getLandmarksNum(); j++)
            {
                linear = new Linear();
                String var = "X" + i + "," + j;
                linear.add(1, var);
                var = "Y" + i;
                linear.add(-1, var);
                problem.add(linear, "<=", 0);
            }

        }

//            /**
//             * Part 3: for each i,j Yi>= (aveDistance[i]/sumDistance) * R
//             */
//            for(int i = 0 ; i < Simulator.system.getLandmarksNum() ; i++)
//            {
//                linear = new Linear();
//                String var = "Y"+i;
//                linear.add(1, var);
//                problem.add(linear, ">=", ((sumDistance - aveDistance[i])/sumDistance) * Simulator.system.getReplicationDegree());
//            }

        /**
         * Part 3: Sigma(i)Xij = 1
         */
        for (int j = 0; j < SkipSimParameters.getLandmarksNum(); j++)
        {
            linear = new Linear();
            for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
            {
                String var = "X" + i + "," + j;
                linear.add(1, var);
            }
            problem.add(linear, "=", Math.min(SkipSimParameters.getReplicationDegree(), SkipSimParameters.getLandmarksNum()));
        }

        /**
         * Part 4: Sigma(i)Yi = MNR
         */
        linear = new Linear();
        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            String var = "Y" + i;
            linear.add(1, var);
        }
        problem.add(linear, "=", SkipSimParameters.getReplicationDegree());
//
//            /**
//             * Part 5: for each i, Yi is integer
//             */
//            for(int i = 0 ; i < Simulator.system.getLandmarksNum() ; i++)
//                {
////                    linear = new Linear();
//                    String var = "Y"+i;
//                    problem.setVarType(var, Integer.class);
//
////                    linear.add(1, var);
////                    problem.add(linear, "<=", 1);
////
////                    linear = new Linear();
////                    linear.add(1, var);
////                    problem.add(linear, ">=", 0);
//                }

        /**
         * Part 6: for each i,j, 0<=Xi,j<=1, Xi,j is integer
         */
        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
            for (int j = 0; j < SkipSimParameters.getLandmarksNum(); j++)
            {

                linear = new Linear();
                String var = "X" + i + "," + j;
                linear.add(1, var);
                problem.add(linear, "<=", 1);

                linear = new Linear();
                linear.add(1, var);
                problem.add(linear, ">=", 0);

                problem.setVarType(var, Integer.class);
            }

//            /**
//             * Part 7: Sigma(j)Xij >= Yi
//             */
//            for(int i = 0 ; i < Simulator.system.getLandmarksNum() ; i++)
//            {
//                linear = new Linear();
//                for(int j = 0 ; j < Simulator.system.getLandmarksNum() ; j++)
//                {
//                    String var = "X"+i+","+j;
//                    linear.add(-1, var);
//                }
//
//                String var = "Y"+i;
//                linear.add(1, var);
//                problem.add(linear, "<=", 0);
//            }


        /**
         * Part 4: Set the type Yi and Yi >=0
         */
        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            String var = "Y" + i;
            problem.setVarType(var, Integer.class);

            linear = new Linear();
            var = "Y" + i;
            linear.add(1, var);
            problem.add(linear, ">=", 0);

//                    linear = new Linear();
//                    var = "Y"+i;
//                    linear.add(1, var);
//                    problem.add(linear, "<=", 0.3 * Simulator.system.getReplicationDegree());
        }


        /**
         * Solving the problem
         */
        Solver solver = factory.get(); // you should use this solver only once for one problem
        Result result = solver.solve(problem);
        //System.out.println(problem.toString());

            /*
            Extracting the results
             */
        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            String var = "Y" + i;
            repShares[i] = result.get(var).intValue();
        }

        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            System.out.println(repShares[i] + " " + i + " " + sgo.getTG().mLandmarks.getDynamicPrefix(i) + " " + (int) aveDistance[i] + " " + (int) aveDistanceToLandmarks[i]);
        }
    }

    /**
     * Should only be used by LARAS, the implementation of our NOMS paper
     *
     */
    public void populationBasedRepShareDefining()
    {
        int[] regionsPopulation = new int[SkipSimParameters.getLandmarksNum()];


        for (int i = 0; i < SkipSimParameters.getSystemCapacity(); i++)
        {
            //Todo suspicious if statement
            if ((i < SkipSimParameters.getDataRequesterNumber() && !isPublicReplication()) || isPublicReplication()) regionsPopulation[sgo.getTG().mNodeSet.ClosestLandmark(i, sgo.getTG().mLandmarks)] += 1;
        }

        double sum = 0;
        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            sum = sum + regionsPopulation[i];
        }

        sum = SkipSimParameters.getReplicationDegree() / sum;

        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            repShares[i] = (int) Math.round(sum * regionsPopulation[i]);
        }
        sum = 0;
        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            sum = sum + repShares[i];
        }

        while (sum < SkipSimParameters.getReplicationDegree())
        {
            int minIndex = 0;
            double min = Double.MAX_VALUE;
            for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
                if (regionsPopulation[i] < min && repShares[i] < 1)
                {
                    min = regionsPopulation[i];
                    minIndex = i;
                }
            repShares[minIndex] = repShares[minIndex] + 1;
            if (repShares[minIndex] < 0) repShares[minIndex] = 0;

            sum = 0;
            for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
            {
                sum = sum + repShares[i];

            }
        }


        sum = 0;
        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            sum = sum + repShares[i];
        }

        while (sum > SkipSimParameters.getReplicationDegree())
        {
            int minIndex = 0;
            double min = Double.MAX_VALUE;
            for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
                if (repShares[i] < min && repShares[i] > 0)
                {
                    min = repShares[i];
                    minIndex = i;
                }
            repShares[minIndex] = repShares[minIndex] - 1;
            if (repShares[minIndex] < 0) repShares[minIndex] = 0;

            sum = 0;
            for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
            {
                sum = sum + repShares[i];

            }
        }

        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            System.out.println(repShares[i] + " " + i + " " + regionsPopulation[i]);
        }
    }

    /**
     * Should only be used by LARAS, the implementation of our NOMS paper
     *
     */
    public void adaptivePopulationBasedSubproblemSizeDefining()
    {
        double[] regionsPopulation = new double[SkipSimParameters.getLandmarksNum()];
        for (int i = 0; i < SkipSimParameters.getSystemCapacity(); i++)
        {
            //Todo suspicious if statement
            if ((i < SkipSimParameters.getDataRequesterNumber() && isPublicReplication()) || !isPublicReplication()) regionsPopulation[sgo.getTG().mNodeSet.ClosestLandmark(i, sgo.getTG().mLandmarks)] += 1;
        }

        double sum = 0;
        int maxIndex = 0;
        double max = Double.MIN_VALUE;
        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            if (regionsPopulation[i] > max)
            {
                maxIndex = i;
                max = regionsPopulation[i];
            }
            sum = sum + regionsPopulation[i];
        }


        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            regionsPopulation[i] = (regionsPopulation[i] / max);
        }

        double expectedPopulation = SkipSimParameters.getSystemCapacity() / SkipSimParameters.getLandmarksNum();
        //double expectedReplicas   = Simulator.system.MNR  / Simulator.system.landmarks;

        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            adaptiveSubproblemSizes[i] = (int) Math.round(regionsPopulation[i] * expectedPopulation * Math.log(SkipSimParameters.getReplicationDegree()) / Math.log(2));
            //adaptiveSubproblemSizes[i] = (int) Math.round(regionsPopulation[i] * expectedPopulation);
        }

        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            int subProblemSize = 2;
            while (subProblemSize < adaptiveSubproblemSizes[i] && subProblemSize < SkipSimParameters.getSystemCapacity())
                subProblemSize *= 2;
            if (subProblemSize > SkipSimParameters.getSystemCapacity() / 2) subProblemSize /= 2;
            while (subProblemSize < 16) subProblemSize *= 2;
            adaptiveSubproblemSizes[i] = subProblemSize;
        }

        System.out.println("Adaptive Sub-problem defining: ");
        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            System.out.println("Population based Sub-problem size = " + adaptiveSubproblemSizes[i] + "\t SkipGraph.Landmarks Index = " + i);
        }
    }

    protected double latencyToClosestLandmark(int index)
    {
        double minLatency = Double.MAX_VALUE;
        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            if (sgo.getTG().mLandmarks.getLandmarkCoordination(i).distance(sgo.getTG().mLandmarks.getLandmarkCoordination(index)) < minLatency && i != index)
                minLatency = sgo.getTG().mLandmarks.getLandmarkCoordination(i).distance(sgo.getTG().mLandmarks.getLandmarkCoordination(index));
        }

        return minLatency;
    }

    /*
    For this function to work properly repshares[] should be first determined
     */
    public void adaptiveSubproblemSizeDefining(int minSubProblemSize)
    {
        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            adaptiveSubproblemSizes[i] = Math.max(minSubProblemSize, repShares[i]);
        }

        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            int subProblemSize = 2;
            while (subProblemSize < adaptiveSubproblemSizes[i] && subProblemSize < SkipSimParameters.getSystemCapacity())
                subProblemSize *= 2;
            adaptiveSubproblemSizes[i] = subProblemSize;
        }

        /*
        Average latency of all SkipGraph.Nodes in the Simulator.system to each landmark which comes directly from the HDPAD or DPAD
         */
        double[] averageLatency = averageDistanceOfLandmarks();

        /*

         */
        System.out.println("Adaptive Sub-problem defining: ");
        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            System.out.println("Sub-problem size = " + adaptiveSubproblemSizes[i] + "\t SkipGraph.Landmarks Index = " + i + " Population " + regionPopulation[i] + " prefix " + sgo.getTG().mLandmarks.dynamicPrefixLength(i) + " average latency to SkipGraph.Nodes " + (int) averageLatency[i] + " total latency to landmarks " + (int) totalLatencyToLandmark(i) + " min latency to a landmark " + (int) latencyToClosestLandmark(i));
        }
    }


    public void setCorrespondingReplica(int dataOwnerID)
    {
        for (int i = 0; i < SkipSimParameters.getSystemCapacity(); i++)
        {
            if (((Node) sgo.getTG().mNodeSet.getNode(i)).getReplicaIDSet().contains(dataOwnerID))
            {
                ((Node) sgo.getTG().mNodeSet.getNode(i)).setDataRequesterID(dataOwnerID);
                ((Node) sgo.getTG().mNodeSet.getNode(i)).setCorrespondingReplica(i);
                continue;
            }

            int closestReplica = 0;
            double closestReplicaDistance = Integer.MAX_VALUE;
            for (int j = 0; j < SkipSimParameters.getSystemCapacity(); j++)
            {
                if (((Node) sgo.getTG().mNodeSet.getNode(j)).getReplicaIDSet().contains(dataOwnerID))
                    if (((Node) sgo.getTG().mNodeSet.getNode(i)).getCoordinate().distance(((Node) sgo.getTG().mNodeSet.getNode(j)).getCoordinate()) < closestReplicaDistance)
                    //if (realDistance[i][j] < closestReplicaDistance)
                    {
                        closestReplica = j;
                        closestReplicaDistance = ((Node) sgo.getTG().mNodeSet.getNode(i)).getCoordinate().distance(((Node) sgo.getTG().mNodeSet.getNode(j)).getCoordinate());
                        //closestReplicaDistance = realDistance[i][j];
                    }
            }

            if (SkipSimParameters.getReplicationType().equals("public") || i < SkipSimParameters.getDataRequesterNumber())
            {
                ((Node) sgo.getTG().mNodeSet.getNode(i)).setDataRequesterID(dataOwnerID);
                ((Node) sgo.getTG().mNodeSet.getNode(i)).setCorrespondingReplica(closestReplica);
            }

        }
    }

    protected void dataRequesterPopulation()
    {
        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            regionPopulation[i] = 0;
            dataRequesterNumbers[i] = 0;
        }
        for (int i = 0; i < SkipSimParameters.getSystemCapacity(); i++)
        {
            regionPopulation[ClosestLandmark((Node) sgo.getTG().mNodeSet.getNode(i))]++;
            //if (repType == SkipSimParameters.PRIVATE_REPLICATION && i < SkipSimParameters.getDataRequesterNumber())
            if (!isPublicReplication() && i < SkipSimParameters.getDataRequesterNumber())
            {
                dataRequesterNumbers[ClosestLandmark((Node) sgo.getTG().mNodeSet.getNode(i))]++;
            }
        }

    }

    private int ClosestLandmark(Node n)
    {
        double min = Double.MAX_VALUE;
        int index = 0;
        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            if (n.getCoordinate().distance(sgo.getTG().mLandmarks.getLandmarkCoordination(i)) < min)
            {
                min = n.getCoordinate().distance(sgo.getTG().mLandmarks.getLandmarkCoordination(i));
                index = i;
            }
        }
        return index;
    }

    public void delayBasedReplication(double delayBound, int initialReplicationDegree, SkipGraphOperations sgo, String algorithmName, int dataOwnerIndex)
    {
        System.out.println("Delay based replication just started!");
        int replicationDegree = initialReplicationDegree;
        double averageDelay;
        do
        {
            reset();
            SkipSimParameters.setReplicationDegree(replicationDegree);
            averageDelay = Algorithm(sgo, dataOwnerIndex);
            System.out.println("Replication degree " + replicationDegree + " average delay " + averageDelay);
            if (averageDelay >= delayBound)
            {
                replicationDegree++;
            }

        } while (averageDelay > delayBound);
        replicationDegreeDataBase[SkipSimParameters.getCurrentTopologyIndex() - 1] = replicationDegree;
        if (SkipSimParameters.getCurrentTopologyIndex() == SkipSimParameters.getTopologyNumbers())
        {
            double average = (double) IntStream.of(replicationDegreeDataBase).sum() / SkipSimParameters.getTopologyNumbers();
            double SD = 0;
            for (int i = 0; i < SkipSimParameters.getTopologyNumbers(); i++)
            {
                SD += Math.pow(replicationDegreeDataBase[i] - average, 2);
            }
            SD = SD / SkipSimParameters.getTopologyNumbers();
            SD = Math.sqrt(SD);
            System.out.println("Delay bound simulation to obtain average bount of " + delayBound + " ms " + " \n Average replication degree " + average + "\n algorithm name " + algorithmName + " SD " + SD + " NOR = " + SkipSimParameters.getDataRequesterNumber());
        }


    }

    public boolean isPublicReplication()
    {
        return SkipSimParameters.getReplicationType().equalsIgnoreCase(Constants.Replication.Type.PUBLIC);
    }


}
