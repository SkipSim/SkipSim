package StaticReplication;

import Simulator.SkipSimParameters;
import SkipGraph.Node;
import SkipGraph.SkipGraphOperations;

public class Rep_Alg09_PrivateRepOnNeighbors extends StaticReplication
{

    private void ReplicateOnNeighborsGenerator(int dataOwnerIndex)
        {


            int index = 0;
            while (index < SkipSimParameters.getSystemCapacity() && ((Node) sgo.getTG().mNodeSet.getNode(index)).neighborNumber() < SkipSimParameters.getReplicationDegree()) index++;

            if (index >= SkipSimParameters.getSystemCapacity())
                {
                    System.out.println("Error in Rep_Alg03_RepOnNeighbors.java: there is no SkipGraph.Node in the Simulator.system with " + SkipSimParameters.getReplicationDegree() + " neighbors" + "\n Change MNR please");
                    System.exit(0);
                }

            for (int i = 0, rep = 0; i < SkipSimParameters.getLookupTableSize() && rep <= SkipSimParameters.getReplicationDegree(); i++)
                for (int j = 0; j < 2; j++)
                    {
                        if (sgo.getTG().mNodeSet.getNode(index).getLookup(i, j) != -1 && !((Node) sgo.getTG().mNodeSet.getNode(index)).getReplicaIDSet().contains(dataOwnerIndex))
                            {
                                ((Node) sgo.getTG().mNodeSet.getNode(sgo.getTG().mNodeSet.getNode(index).getLookup(i, j))).setReplicaIDSet(dataOwnerIndex);
                                //realWorldReplicaSet[sgo.getTG().mNodeSet.getNode(index).getLookup(i, j)] = true;
                                rep++;
                            }
                    }


            setCorrespondingReplica(dataOwnerIndex);

        }


    @Override
    public double Algorithm(SkipGraphOperations inputSgo, int dataOwnerIndex)
        {
            sgo = inputSgo;
            reset();
            double ratio = 0;
            //for (int i = 0; i < getExperimentNumber(); i++)
            //{


            //repTools.replicaSetGenerator(repTools.PrivateOptimizer(repTools.realDistance, repTools.getProblemSize()), "Real", repTools.getProblemSize());
            ReplicateOnNeighborsGenerator(dataOwnerIndex);
            //double localDelay = privateTotalDelay();
            double averageAccessDelay = privateAverageDelay();
            //int realDelay  = repTools.privateTotalDelay(repTools.realReplicaAssignment);

            resetRep();

            //ratio = ratio + (double) localDelay; ///realDelay;
            //}
            //ratio = ratio / getExperimentNumber();
            //System.out.println("Average Delay " + ratio);
            //setRatioDataSet(Simulator.system.getCurrentTopologyIndex() - 1, ratio);
            //double averageAccessDelay = localDelay / Simulator.system.getDataRequesterNumber();
            System.out.println("Average Delay " + averageAccessDelay);
            setRatioDataSet(SkipSimParameters.getCurrentTopologyIndex() - 1, averageAccessDelay);

            if (SkipSimParameters.getCurrentTopologyIndex() == SkipSimParameters.getTopologyNumbers() && !SkipSimParameters.isDelayBasedSimulaton())
                {
                    evaluation(" Algorithm 09 PrivateRepOnNeighbors ");
                }


            return averageAccessDelay;
        }
}