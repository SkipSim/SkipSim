//import time.LocalTime;
//import java.util.ArrayList;
//
//
//public class Rep_Alg16_PrivateLARAS2 extends StaticReplication {
//
//    @Override
//    public double Algorithm(SkipGraph.SkipGraphOperations inputSgo)
//        {
//
//            sgo = inputSgo;
//            //reset();
//            updateRegionsPopulation(Simulator.system.PRIVATE_REPLICATION);
//            repShareDefining(Simulator.system.PRIVATE_REPLICATION, 32.01, 33.88, 34.11);
//            //improvedRepShare(Simulator.system.PRIVATE_REPLICATION);
//            adaptiveSubproblemSizeDefining(16);
//            tablesInit();
//            replicaSetInit();
//
//            //repTools.replicaSetGenerator(repTools.PublicOptimizer(repTools.realDistance, repTools.getProblemSize()), "Real", repTools.getProblemSize());
//
//            //replicaAssignmentSetGenerator(getProblemSize());
//            //assignToOther();
//            //double localDelay = privateTotalDelay();
//            //printReplicationTables(sgo);
//            //int realDelay  = repTools.publicTotalDelay(repTools.realReplicaAssignment);
//            //System.out.println("Real Delay " + localDelay);
//            //System.out.println("Local Delay " + realDelay);
//            double averageAccessDelay = regionWideOptimization();
//
//            //double averageAccessDelay = (double) localDelay / Simulator.system.getDataRequesterNumber(); ///realDelay;
//            //double averageAccessDelay = privateAverageDelay();
//            //System.out.println("Average Delay " + averageAccessDelay + " Run " + Simulator.system.getCurrentTopologyIndex());
//            setRatioDataSet(Simulator.system.getCurrentTopologyIndex() - 1, averageAccessDelay);
//
//            //repEvaluation.publicReplicationLoadAnalysis(repTools.getProblemSize(), repTools.realWorldReplicaAssignment);
//            if (Simulator.system.getCurrentTopologyIndex() == Simulator.system.getTopologyNumbers() && !Simulator.system.isDelayBasedSimulaton())
//                {
//                    //repEvaluation.loadEvaluation();
//                    evaluation(" Algorithm 13 Private LARAS2 ");
//                    System.out.println(LocalTime.now());
//                }
//
//            return averageAccessDelay;
//        }
//
//
//    /*
//    Note: before calling this function the subProblemNameIDSize and subProblemSize should be updated accordingly.
//     */
//    private void nameidsDistanceGenerator()
//        {
//            nameidsDistance = new int[getSubProblemSize()][getSubProblemSize()];
//            for (int i = 0; i < getSubProblemSize(); i++)
//                for (int j = 0; j < getSubProblemSize(); j++)
//                    {
//                        if (i == j)
//                            {
//                                nameidsDistance[i][j] = 0;
//                            }
//                        else nameidsDistance[i][j] = Math.max(1, Simulator.system.getNameIDLength() - commonPrefixLength(i, j));
//                    }
//        }
//
//    private double regionWideOptimization()
//        {
//
//            boolean[] oldRepSet = new boolean[Simulator.system.getSystemCapacity()];
//            double[] oldRepAccuracy = new double[Simulator.system.getSystemCapacity()];
//            int[] oldSubProblemRepSet = new int[Simulator.system.getSystemCapacity()];
//            boolean[] newRepSet = new boolean[Simulator.system.getSystemCapacity()];
//            double[] newRepAccuracy = new double[Simulator.system.getSystemCapacity()];
//            int[] newSubProblemRepSet = new int[Simulator.system.getSystemCapacity()];
//
//            System.out.println("Private LARAS2 has started.....");
//            for (int i = 0; i < Simulator.system.getLandmarksNum(); i++)
//                {
//                    badCandidates = new ArrayList<>();
//                    setReplicationDegree(getRepshare(i));
//                    if (getReplicationDegree() == 0) continue;
//                    setAdaptiveSubProblemSize(i);
//                    nameidsDistanceGenerator();
//
//                    double oldAc = replicaSetGenerator4(ILP(nameidsDistance, getSubProblemSize(), Simulator.system.PRIVATE_REPLICATION), getSubProblemSize(), i, oldRepSet, oldRepAccuracy, oldSubProblemRepSet);
//                    refineBadCandidates(oldRepSet, oldRepAccuracy, oldSubProblemRepSet);
//                    double newAc = replicaSetGenerator4(ILP(nameidsDistance, getSubProblemSize(), Simulator.system.PRIVATE_REPLICATION), getSubProblemSize(), i, newRepSet, newRepAccuracy, newSubProblemRepSet);
//                    int loopCounter = 0;
//                    while (oldAc < newAc)
//                        {
//                            System.arraycopy(newRepSet, 0, oldRepSet, 0, oldRepSet.length);
//                            System.arraycopy(newRepAccuracy, 0, oldRepAccuracy, 0, oldRepAccuracy.length);
//                            oldAc = newAc;
//                            refineBadCandidates(newRepSet, newRepAccuracy, newSubProblemRepSet);
//                            newAc = replicaSetGenerator4(ILP(nameidsDistance, getSubProblemSize(), Simulator.system.PRIVATE_REPLICATION), getSubProblemSize(), i, newRepSet, newRepAccuracy, newSubProblemRepSet);
//                            loopCounter++;
//                        }
//
//                    for (int j = 0; j < Simulator.system.getSystemCapacity(); j++)
//                        {
//                            if (oldRepSet[j])
//                                {
//                                    sgo.getTG().mNodeSet.getNode(j).setReplicaIDSet(true);
//                                }
//
//                        }
//
//
//                    //System.out.println(repStatus);
//                    //validityTest();
//                    //realWordTransform(i);
//                    localReplicaSetInit();
//                }
//
//            setCorrespondingReplica();
//            double averageAccessDelay = privateAverageDelay();
//
//            //double ratio = (double) localDelay / Simulator.system.getSystemCapacity(); ///realDelay;
//            System.out.println("Average Delay " + averageAccessDelay + " Run " + Simulator.system.getCurrentTopologyIndex());
//            return averageAccessDelay;
//        }
//}