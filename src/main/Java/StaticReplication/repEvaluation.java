package StaticReplication;

import Simulator.SkipSimParameters;
import SkipGraph.Node;
import SkipGraph.SkipGraphOperations;

import static Simulator.Parameters.REPLICATION_TIME_INTERVAL;

public class repEvaluation extends StaticReplication
{
	private double[] loadDataSet     = new double[SkipSimParameters.getTopologyNumbers()];
	private static double[] topologyAverageAvailableReplicas;
	private static double[] simulationAverageAvailableReplicas;

	//private static int[] numOfSamples;
	//private static double[] numOfRepForThisTopo;

	//private static boolean topologyInit = true;
	//private static boolean datasetInit  = true;

	public void privateReplicationLoadAnalysis(int problemSize, boolean[][] set)
    {
   	 int[] replicaLoad = new int[problemSize];
   	 for(int i = 0 ; i < problemSize ; i++)
   		 replicaLoad[i] = 0;
   	 for(int i = 0 ; i < problemSize ; i++)
   		 for(int j = 0 ; j < problemSize ; j++)
   		 {
   			 if(set[i][j] && j < SkipSimParameters.getDataRequesterNumber())
   			 {
   				 replicaLoad[i] = replicaLoad[i] + 1;
   			 }
   		 }
   	 double ave = 0;
   	 for(int i = 0; i < problemSize ; i++)
   	 {
   		 ave +=replicaLoad[i];
   		 //System.out.println(replicaLoad[i] + " ");
   	 }

   	ave  =  ave / SkipSimParameters.getReplicationDegree();

   	 double sd = 0;
   	 for(int i = 0; i < problemSize ; i++)
   	 {
   		 if(replicaLoad[i] > 0)
   			 sd += Math.pow(replicaLoad[i] - ave, 2);

   	 }

   	 //System.out.println("Sum of SD " + sd + " " + ave);
   	 sd = sd / SkipSimParameters.getReplicationDegree();
   	 //System.out.println("ave of SD " + sd);
   	 sd = Math.sqrt(sd);


   	 loadDataSet[SkipSimParameters.getCurrentTopologyIndex() - 1] = sd;
   	 //System.out.println("SD Load on a replica " + (int) ave + " " + (int) sd);
    }

	public void publicReplicationLoadAnalysis(int problemSize, boolean[][] set)
    {
   	 int[] replicaLoad = new int[problemSize];
   	 for(int i = 0 ; i < problemSize ; i++)
   		 replicaLoad[i] = 0;
   	 for(int i = 0 ; i < problemSize ; i++)
   		 for(int j = 0 ; j < problemSize ; j++)
   		 {
   			 if(set[i][j])
   			 {
   				 replicaLoad[i] = replicaLoad[i] + 1;
   			 }
   		 }
   	 double ave = 0;
   	 for(int i = 0; i < problemSize ; i++)
   	 {
   		 ave +=replicaLoad[i];
   		 //System.out.println(replicaLoad[i] + " ");
   	 }

   	ave  =  ave / SkipSimParameters.getReplicationDegree();

   	 double sd = 0;
   	 for(int i = 0; i < problemSize ; i++)
   	 {
   		 if(replicaLoad[i] > 0)
   			 sd += Math.pow(replicaLoad[i] - ave, 2);

   	 }

   	 //System.out.println("Sum of SD " + sd + " " + ave);
   	 sd = sd / SkipSimParameters.getReplicationDegree();
   	 //System.out.println("ave of SD " + sd);
   	 sd = Math.sqrt(sd);


   	 loadDataSet[SkipSimParameters.getCurrentTopologyIndex() - 1] = sd;
   	 //System.out.println("SD Load on a replica " + (int) ave + " " + (int) sd);
    }

    public void loadEvaluation()
    {

	   	 double ave  = 0;
	   	 double sd   = 0;
	   	 for(int i = 0; i < SkipSimParameters.getTopologyNumbers() ; i++)
	   	 {
	   		 ave +=loadDataSet[i];
	   		 //System.out.print(loadDataSet[i] + " ");
	   	 }

	     //System.out.println("Sum of ave " + ave);
	   	 ave  = ave / SkipSimParameters.getTopologyNumbers();

	   	 for(int i = 0; i < SkipSimParameters.getTopologyNumbers() ; i++)
	   	 {
	   			 sd += Math.pow(loadDataSet[i] - ave, 2);
	   	 }

	   	 sd = sd / SkipSimParameters.getTopologyNumbers();
	   	 sd = Math.sqrt(sd);

	   	 System.out.println("The average load on a replica is " + ave + " with the SD of " +  sd );
    }



    public static void numberOfOnlineReplicasEvaluation(int currentTime, SkipGraphOperations sgo, String algorithmName)
    {
		int lastReplicationTime = SkipSimParameters.getReplicationTime() + (REPLICATION_TIME_INTERVAL * (SkipSimParameters.getDataOwnerNumber() - 1));
    	if(currentTime == lastReplicationTime + 1)
		{
			/*
			Initializing for the entire simulation at the beginning of the first topology
			 */
			if(SkipSimParameters.getCurrentTopologyIndex() == 1)
			{
				simulationAverageAvailableReplicas = new double[SkipSimParameters.getTopologyNumbers()];
			}
			/*
			initializing for this topology at the t = 0
			 */
			topologyAverageAvailableReplicas = new double[SkipSimParameters.getLifeTime()];
		}

		//Replication time of the last dataowner
//		int lastReplicationTime = system.getReplicationTime() + (10 * system.getDataOwnerNumber() - 1);
    	double[] dataOwnerReplicas = new double[SkipSimParameters.getDataOwnerNumber()];
    	for(int i = 0; i < SkipSimParameters.getSystemCapacity() ; i++)
		{
			for(int j = 0; j < SkipSimParameters.getDataOwnerNumber(); j ++)
			{
				if(((Node) sgo.getTG().mNodeSet.getNode(i)).getReplicaIDSet().contains(j) && ((Node) sgo.getTG().mNodeSet.getNode(i)).isOnline())
				{
					dataOwnerReplicas[j]++;
				}
			}
		}

		/*
		average number of online replicas per each data owner
		 */
		double average = 0;
		for(int i = 0; i < SkipSimParameters.getDataOwnerNumber() ; i++)
		{
			average += dataOwnerReplicas[i];
		}

		topologyAverageAvailableReplicas[currentTime] = average / SkipSimParameters.getDataOwnerNumber();

		/*
		If we are at the end of the current topology and hence to conclude this topology
		 */
		if(currentTime == SkipSimParameters.getLifeTime() - 1) // && system.getCurrentTopologyIndex() != system.getTopologyNumbers())
		{
			average = 0;
			for (int t = lastReplicationTime + 1; t < SkipSimParameters.getLifeTime(); t++)
			{
				average += topologyAverageAvailableReplicas[t];
			}
			simulationAverageAvailableReplicas[SkipSimParameters.getCurrentTopologyIndex() - 1] = average / (SkipSimParameters.getLifeTime() - lastReplicationTime - 1);

		/*
		if we are at the end of the last topology and hence to conclude the results
		 */
			if (SkipSimParameters.getCurrentTopologyIndex() == SkipSimParameters.getTopologyNumbers())
			{
				average = 0;
				double sd = 0;
				for (int topologyIndex = 0; topologyIndex < SkipSimParameters.getTopologyNumbers(); topologyIndex++)
				{
					average += simulationAverageAvailableReplicas[topologyIndex];
				}

				average /= SkipSimParameters.getTopologyNumbers();

				for (int topologyIndex = 0; topologyIndex < SkipSimParameters.getTopologyNumbers(); topologyIndex++)
				{
					sd += Math.pow(average - simulationAverageAvailableReplicas[topologyIndex], 2);
				}

				sd /= SkipSimParameters.getTopologyNumbers();
				sd = Math.sqrt(sd);
				System.out.println("----------------------------------------------------------");
				System.out.println("RepEvaluation.java: Dynamic Replication Evaluation. Replication Algorithm: " + algorithmName);
				System.out.println("Replication degree " + SkipSimParameters.getReplicationDegree());
				System.out.println("Average availability of replicas: " + average + ", standard deviation: " + sd);
				System.out.println("Note: Average and standard deviation are taken over all topologies");
				System.out.println("----------------------------------------------------------");
			}
		}


    }


	 private double     averagePairCorrelation_ForThisTopology = 0;
	 private double     averagePairCorrelation = 0;
//TODO awake's implementation
//	 public void pairCorrelation(Nodes mNodeSet)
//	 {
//		 averagePairCorrelation_ForThisTopology = 0;
//
//
//		 for(int i = 0 ; i < system.getSystemCapacity() ; i++)
//		 {
//
//			 if(getDynamicRealWorldReplicaSet(i))
//			 {
//				 double correlation = 0;
//				 int counter = 0;
//				 for(int j = 0 ; j < system.getSystemCapacity() ; j++)
//				 {
//					 if(getDynamicRealWorldReplicaSet(i) && i != j)
//					 {
//						 for(int t = 0 ; t < system.getTimeSlot() ; t++)
//						 {
//							 correlation += (double) (mNodeSet.getNode(i).getAvailabilityVector(t) * mNodeSet.getNode(i).getAvailabilityVector(t));
//							 counter ++;
//						 }
//					 }
//				 }
//				 averagePairCorrelation_ForThisTopology += (double)(correlation/counter);
//			 }
//		 }
//
//		 averagePairCorrelation += (double)(averagePairCorrelation_ForThisTopology / system.getReplicationDegree());
//		 if(system.getCurrentTopologyIndex() == system.getTopologyNumbers())
//		 {
//			 System.out.println("The average pair correlation was " + averagePairCorrelation / system.getTopologyNumbers());
//		 }
//	 }
//
//	 private double     averageCorrelation_ForThisTopology = 0;
//	 private double     averageCorrelation = 0;
//	 public void repCorrelation(Nodes mNodeSet)
//	 {
//		 averageCorrelation_ForThisTopology = 0;
//		 double[] correlations = new double[system.getTimeSlot()];
//		 int counter = 0;
//		 for(int i = 0 ; i < system.getSystemCapacity() ; i++)
//		 {
//			 if(getDynamicRealWorldReplicaSet(i))
//			 {
//				 if(counter == 0)
//				 {
//					 for(int t = 0 ; t < system.getTimeSlot() ; t++)
//					 {
//						 correlations[t] = (double) (mNodeSet.getNode(i).getAvailabilityVector(t));
//						 counter++;
//					 }
//				 }
//				 else
//				 {
//					 for(int t = 0 ; t < system.getTimeSlot() ; t++)
//					 {
//						 correlations[t] *= (double) (mNodeSet.getNode(i).getAvailabilityVector(t));
//					 }
//				 }
//			 }
//		 }
//
//		 for(int t = 0 ; t < system.getTimeSlot() ; t++)
//		 {
//			 averageCorrelation_ForThisTopology += correlations[t];
//		 }
//
//
//
//		 averageCorrelation += (double)(averageCorrelation_ForThisTopology);
//		 if(system.getCurrentTopologyIndex() == system.getTopologyNumbers())
//		 {
//			 System.out.println("The average correlation was " + averageCorrelation / system.getTopologyNumbers());
//		 }
//	 }

	@Override
	public double Algorithm(SkipGraphOperations sgo, int dataOwnerIndex)
		{
			return 0;
		}

}
