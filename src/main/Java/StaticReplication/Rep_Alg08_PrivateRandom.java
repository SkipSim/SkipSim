package StaticReplication;

import Simulator.SkipSimParameters;
import SkipGraph.Node;
import SkipGraph.SkipGraphOperations;

import java.util.Random;


public class Rep_Alg08_PrivateRandom extends StaticReplication
    {


        public void randomReplicaGenerator(int dataOwnerID)
            {
                Random random = new Random();
                for (int i = 0; i < SkipSimParameters.getReplicationDegree(); i++)
                    {
                        int index = random.nextInt(SkipSimParameters.getSystemCapacity() - 1);
                        while (((Node) sgo.getTG().mNodeSet.getNode(index)).getReplicaIDSet().contains(dataOwnerID)) index = random.nextInt(SkipSimParameters.getSystemCapacity() - 1);

                        ((Node) sgo.getTG().mNodeSet.getNode(index)).setReplicaIDSet(dataOwnerID);
                    }

                setCorrespondingReplica(dataOwnerID);

            }

        @Override
        public double Algorithm(SkipGraphOperations inputSgo, int dataOwnerID)
            {
                sgo = inputSgo;
                System.out.println("The Private Randomized Replication Started....");
                //repTools.reset();
                reset();
                //double ratio = 0;


                //repTools.replicaSetGenerator(repTools.PrivateOptimizer(repTools.realDistance, repTools.getProblemSize()), "Real", repTools.getProblemSize());
                randomReplicaGenerator(dataOwnerID);
                double averageAccessDelay = privateAverageDelay();
                //double averageAccessDelay = privateTotalDelay()/Simulator.system.getDataRequesterNumber();
                //int realDelay  = repTools.privateTotalDelay(repTools.realReplicaAssignment);

                //ratio = ratio + (double) localDelay; ///realDelay;

                resetRep();


                //ratio = ratio / getExperimentNumber();
                System.out.println("Average Delay " + averageAccessDelay);
                setRatioDataSet(SkipSimParameters.getCurrentTopologyIndex() - 1, averageAccessDelay);

                if (SkipSimParameters.getCurrentTopologyIndex() == SkipSimParameters.getTopologyNumbers() && !SkipSimParameters.isDelayBasedSimulaton())
                    {
                        evaluation(" Algorithm 08 PrivateRandom ");
                    }
                return averageAccessDelay;
            }
    }