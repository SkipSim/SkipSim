package StaticReplication;

import java.util.Arrays;
import java.util.Random;

import Simulator.SkipSimParameters;
import SkipGraph.Node;
import SkipGraph.SkipGraphOperations;


public class Rep_Alg04_RepOnPath extends StaticReplication
{
    private boolean adaptiveReplication;

    public Rep_Alg04_RepOnPath(boolean adaptiveReplication)
        {
            this.adaptiveReplication = adaptiveReplication;
        }

    private void ReplicateOnPathGenerator(int dataOwnerIndex)
        {
            Random random = new Random();
            int dataOwner = random.nextInt(SkipSimParameters.getSystemCapacity());
            int searchDest = sgo.getTG().mNodeSet.getNode(dataOwner).getNumID();
            int rep = 0 ;
            while(rep <=SkipSimParameters.getReplicationDegree())
                {
                    int searchSource   = random.nextInt(SkipSimParameters.getSystemCapacity());

                    while(searchSource == dataOwner)
                        {
                            searchSource = random.nextInt(SkipSimParameters.getSystemCapacity());
                        }

                    rep = RepOnPath(searchDest, searchSource, rep, false, dataOwnerIndex);

                }

//        int repnum = 0;
//        for(int j = 0 ; j < Simulator.system.getSystemCapacity() ; j++)
//            {
//                if(sgo.getTG().mNodeSet.getNode(j).isReplicaIDSet())
//                    {
//                        repnum++;
//                    }
//            }
//        System.out.println("Rep num " + repnum);
            setCorrespondingReplica(dataOwnerIndex);
        }

    private void AdaptiveReplicationOnPath(int dataOwnerIndex)
        {
            Arrays.fill(pathHistogram, 0);
            Random random = new Random();
            int dataOwner = random.nextInt(SkipSimParameters.getSystemCapacity());
            int searchDest = sgo.getTG().mNodeSet.getNode(dataOwner).getNumID();
            for(int i = 0; i < SkipSimParameters.getSystemCapacity(); i++)
                {
                    RepOnPath(searchDest, i, 0, true, dataOwnerIndex);
                }
            for(int i = 0; i < SkipSimParameters.getReplicationDegree() ; i++)
                {
                    int MaxValue = pathHistogram[0];
                    int MaxIndex = 0;
                    for(int j = 0; j < SkipSimParameters.getSystemCapacity() ; j++)
                        {
                            if(pathHistogram[j] > MaxValue)
                                {
                                    MaxValue = pathHistogram[j];
                                    MaxIndex = j;
                                }
                        }
                    ((Node) sgo.getTG().mNodeSet.getNode(MaxIndex)).setReplicaIDSet(dataOwnerIndex);
                    System.out.println(i + "-index = " + MaxIndex + " histogram " + pathHistogram[MaxIndex]);
                    pathHistogram[MaxIndex] = 0;
                }


            setCorrespondingReplica(dataOwnerIndex);

        }



    @Override
    public double Algorithm(SkipGraphOperations inputSgo, int dataOwnerIndex)
        {
            sgo = inputSgo;
            reset();
            //double ratio = 0;

            //		 printTables();
            //repTools.replicaSetGenerator(repTools.PublicOptimizer(repTools.realDistance, Simulator.system.size), "Real" ,Simulator.system.size);
            if(adaptiveReplication)
                AdaptiveReplicationOnPath(dataOwnerIndex);
            else
                ReplicateOnPathGenerator(dataOwnerIndex);
            //double averageAccessDelay= publicTotalDelay()/Simulator.system.getSystemCapacity();
            double averageAccessDelay = publicAverageDelay();
            //int realDelay    = repTools.publicTotalDelay(repTools.realReplicaAssignment);

            resetRep();
            // ratio = localDelay; ///realDelay;


            System.out.println("Average Delay " + averageAccessDelay);
            setRatioDataSet(SkipSimParameters.getCurrentTopologyIndex() - 1, averageAccessDelay);


            if (SkipSimParameters.getCurrentTopologyIndex() == SkipSimParameters.getTopologyNumbers() && SkipSimParameters.isReplicationLocalityAwarenessEvaluation() && !SkipSimParameters.isDelayBasedSimulaton())
                {
                    evaluation(" Algorithm 04 Rep On Path ");
                }
            //        if (Simulator.system.isReplicationLoadEvaluation()) {
            //            new repEvaluation().loadEvaluation();
            //        }
            return averageAccessDelay;

        }
}