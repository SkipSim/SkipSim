package StaticReplication;

import Simulator.SkipSimParameters;
import SkipGraph.Node;
import SkipGraph.SkipGraphOperations;
import net.sf.javailp.*;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalTime;
import java.util.*;


public class Static_Replication_GLARAS extends StaticReplication
{
    public static final int VIRTUAL_SYSTEM_INITIAL_SIZE = 1;
    static double averageW1 = 0;
    static double averageW2 = 0;
    static double averageW3 = 0;
    private static double numberOfIterations = 0;
    //private static Hashtable<Integer, Integer> overalW1Histogram = new Hashtable<>();
    //private static Hashtable<Integer, Integer> overalW2Histogram = new Hashtable<>();
    //Map<Integer, Map<Integer, Integer>> weightHistogram = new Hashtable<>();
    private static int[][] weightHistorgam = new int[100][100];
    private ArrayList<String> badCandidates;
    private int[] SubProblemRepSet = new int[SkipSimParameters.getSystemCapacity()];
    private boolean[] RepSet = new boolean[SkipSimParameters.getSystemCapacity()];
    private double[] RepAccuracy = new double[SkipSimParameters.getSystemCapacity()];

    @Override
    public double Algorithm(SkipGraphOperations inputSgo, int dataOwnerIndex)
    {
        sgo = inputSgo;
        //reset();
        dataRequesterPopulation();
        //oneRegionRepShare();
        repShareDefining(32.01, 33.88, 34.11);
        //improvedRepShare(Simulator.system.PUBLIC_REPLICATION);
        //adaptiveSubproblemSizeDefining(128);
        //tablesInit();
        //replicaSetInit();

        //double averageAccessDelay = bruteForceOnWeights();
        //double averageAccessDelay = bruteForceOnRegions();
        double averageAccessDelay = regionWideOptimization(dataOwnerIndex);
        setRatioDataSet(SkipSimParameters.getCurrentTopologyIndex() - 1, averageAccessDelay);
        //repEvaluation.publicReplicationLoadAnalysis(repTools.getProblemSize(), repTools.realWorldReplicaAssignment);
        if (SkipSimParameters.getCurrentTopologyIndex() == SkipSimParameters.getTopologyNumbers() && !SkipSimParameters.isDelayBasedSimulaton())
        {
            //repEvaluation.loadEvaluation();
            evaluation(" Algorithm: GLARAS ");
            System.out.println("Average number of iterations for GLARAS is " + numberOfIterations / SkipSimParameters.getTopologyNumbers());
            System.out.println(LocalTime.now());
        }

        return averageAccessDelay;
    }


    private double bruteForceOnRegions(int dataOwnerIndex)
    {
        if (!isPublicReplication())
        {
            throw new IllegalStateException("Static_Replication_GLARAS: bruteForceOnRegions should be only invoked in public replication type: found private replication type");
        }
        printLandmarkPairwiseLatency();
        /*
        closestCounter for each landmark i keeps the number of landmarks that landmark i covers as their closest landmark
        EXCLUDING landmark i itself.
         */
        int[] closestCounter = closestLandmarkCounter();

        /*
        Average latency of all SkipGraph.Nodes in the Simulator.system to each landmark which comes directly from the HDPAD or DPAD
         */
        double[] averageLatency = averageDistanceOfLandmarks();
        Map<String, Integer> resultTable = new Hashtable<>();


        try
        {
            File file = new File("regionZeroBruteforce.txt");
            PrintWriter w = new PrintWriter(file, "UTF-8");

            for (int j = 0; j < SkipSimParameters.getLandmarksNum(); j++)
            {
//                System.out.println("index = " + j
//                        + " prefix length = " + sgo.getTG().mLandmarks.dynamicPrefixLength(j)
//                        + " closest counter =" + closestCounter[j]
//                        + " average latency = " + (int) averageLatency[j]);
//
//                w.println("index = " + j
//                        + " prefix length = " + sgo.getTG().mLandmarks.dynamicPrefixLength(j)
//                        + " closest counter =" + closestCounter[j]
//                        + " average latency = " + (int) averageLatency[j]);
            }
            int counter = 0;
            double minAverage = Double.MAX_VALUE;
            String minAssignment = new String();
            for (int i = 0; i < Math.pow(2, SkipSimParameters.getLandmarksNum()); i++)
            {
                sgo.getTG().mNodeSet.renewReplicationInfo();

                /*
                Initializing rep share of regions with one or more, in case the original replication degree is more
                than number of landmarks.
                 */
                for (int k = 0; k < SkipSimParameters.getLandmarksNum(); k++)
                {
                    setRepShares(k, 1);
                }
                String representation = Integer.toBinaryString(i);

                if (org.apache.commons.lang3.StringUtils.countMatches(representation, "1") == SkipSimParameters.getReplicationDegree())
                {
                    while (representation.length() < SkipSimParameters.getLandmarksNum())
                        representation = "0" + representation;
                    for (int j = 0; j < SkipSimParameters.getLandmarksNum(); j++)
                    {
                        if (Character.getNumericValue(representation.charAt(j)) == 1)
                            setRepShares(j, getRepshare(j) + 1);
                    }

                    double accesDelay = regionWideOptimization(dataOwnerIndex);
                    if (accesDelay < minAverage)
                    {
                        minAverage = accesDelay;
                        minAssignment = representation;
                    }
                    counter++;
                    System.out.println("Run of " + counter + " the average access delay for repshare assignment of " + representation + " is " + (int) accesDelay);
                    w.println("Run of " + counter + " the average access delay for repshare assignment of " + representation + " is " + (int) accesDelay);
                    w.println("***********************************************************************");
                    resultTable.put(representation, (int) accesDelay);
                }
            }
            w.println("************************************************************************");
            w.println("****************The optimal result**************************************");
            w.println("************************************************************************");
            for (int j = 0; j < SkipSimParameters.getLandmarksNum(); j++)
            {
                setRepShares(j, Character.getNumericValue(minAssignment.charAt(j)));
            }

            /*
            Traversing the hash map
             */
            Iterator it = resultTable.entrySet().iterator();
            while (it.hasNext())
            {
                Map.Entry<String, Integer> pair = (Map.Entry) it.next();
                if (pair.getValue().intValue() == (int) minAverage)
                {
                    System.out.println("Simialr accuracy to optimal " + pair.getKey() + " access delay " + pair.getValue().intValue() + "\n\n");
                    w.println("Simialr accuracy to optimal " + pair.getKey() + " access delay " + pair.getValue().intValue() + "\n\n");
                }
            }


            w.close();
            return minAverage;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            // do something
        }

        return -Double.MIN_VALUE;


    }

    private double bruteForceOnWeights(int dataOwnerIndex)
    {
        double[][] results = new double[100][100];
        if (SkipSimParameters.getCurrentTopologyIndex() == 1)
        {
            for (int i = 0; i < 100; i++)
                Arrays.fill(weightHistorgam[i], 0);
        }
        double minAverage = Double.MAX_VALUE;
        double bestW1 = 0;
        double bestW2 = 0;
        double bestW3 = 0;
        //Set<Integer> minimumInterval = new HashSet<>();
        //Hashtable<Integer, Double> results = new Hashtable<>();
        for (double w1 = 0; w1 < 100; w1++)
            for (double w2 = 0; w2 < 100 - w1; w2++)
            {
                double w3 = 100 - w2 - w1;
                sgo.getTG().mNodeSet.renewReplicationInfo();

                for (int k = 0; k < SkipSimParameters.getLandmarksNum(); k++)
                {
                    setRepShares(k, 0);
                }

                repShareDefining(w1 / 100, w2 / 100, w3 / 100);
                double averageAccessDelay = regionWideOptimization(dataOwnerIndex);
                results[(int) w1][(int) w2] = averageAccessDelay;
                //results.put((int) w1, averageAccessDelay);
                if (averageAccessDelay < minAverage)
                {
                    minAverage = averageAccessDelay;
                    bestW1 = w1;
                    bestW2 = w2;
                    bestW3 = w3;
                    //minimumInterval = new HashSet<>();
                }
//                    if(Math.abs(averageAccessDelay - minAverage) < 0.01 * minAverage)
//                        {
//                            //minimumInterval.add((int) w1);
//                        }

                System.out.println("W1 " + w1 + " W2 " + w2 + " W3 " + w3 + " average delay " + averageAccessDelay + " min average " + minAverage);

            }
        //overalInterval.retainAll(minimumInterval);
//            for(Integer i : minimumInterval)
//                {
//                    overalHistogram.put(i, overalHistogram.get(i) + 1 );
//                }
//                Iterator<Map.Entry<Integer, Double>> it = results.entrySet().iterator();
//                while (it.hasNext())
//                    {
//                        Map.Entry<Integer, Double> element = it.next();
//                        if (Math.abs(element.getValue() - minAverage) < 0.01 * minAverage)
//                            {
//                                //overalW1Histogram.put(element.getKey(), overalW1Histogram.get(element.getKey()) + 1);
//
//                            }
//                        //System.out.println("("+element.getKey() +"," + element.getValue() + ")");
//                    }

        for (int i = 0; i < 100; i++)
            for (int j = 0; j < 100; j++)
            {
                if (Math.abs(results[i][j] - minAverage) < 0.01 * minAverage)
                {
                    weightHistorgam[i][j]++;
                }
            }
        averageW1 += bestW1;
        averageW2 += bestW2;
        averageW3 += bestW3;

        if (SkipSimParameters.getCurrentTopologyIndex() == SkipSimParameters.getTopologyNumbers())
        {
            System.out.println("Best Average W1 " + averageW1 / SkipSimParameters.getTopologyNumbers() + " best Average W2 " + averageW2 / SkipSimParameters.getTopologyNumbers() + " best Average W3 " + averageW3 / SkipSimParameters.getTopologyNumbers());
//                        Iterator<Map.Entry<Integer, Integer>> iter = overalHistogram.entrySet().iterator();
//                        while (iter.hasNext())
//                            {
//                                Map.Entry<Integer, Integer> element = iter.next();
//                                System.out.println("(" + element.getKey() + "," + element.getValue() + ")");
//                            }
            for (int i = 0; i < 100; i++)
                for (int j = 0; j < 100; j++)
                {
                    if (100 - i - j >= 0)
                        System.out.println("(" + i + "," + j + "," + (100 - i - j) + "," + weightHistorgam[i][j] + ")");
                }

        }

        return minAverage;


    }

    private double regionWideOptimization(int dataOwnerIndex)
    {


        //repTools.replicaSetGenerator(repTools.PublicOptimizer(repTools.realDistance, repTools.getProblemSize()), "Real", repTools.getProblemSize());
        int virtualSystemSizePower = VIRTUAL_SYSTEM_INITIAL_SIZE;
        System.out.println("GLARAS has started");
        int repCounter = 0;
        int loopCounter = 0;
        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {
            virtualSystemSizePower = VIRTUAL_SYSTEM_INITIAL_SIZE;
            badCandidates = new ArrayList<>();

            if (getRepshare(i) == 0) continue;
            //setAdaptiveSubProblemSize(i);
            //nameidsDistanceGenerator();


            boolean[] BestRepSet = new boolean[SkipSimParameters.getSystemCapacity()];
            double[] BestRepAccuracy = new double[SkipSimParameters.getSystemCapacity()];
            int[] BestSubProblemRepSet = new int[SkipSimParameters.getSystemCapacity()];
            double BestAccuracy = -1;
            Arrays.fill(BestSubProblemRepSet, 0);
            Arrays.fill(BestRepAccuracy, 0);
            Arrays.fill(BestRepSet, false);
            double accuracy;


            int virtualSystemSize = 0;
            int extremeVirturalSystemSize = SkipSimParameters.getSystemCapacity() / SkipSimParameters.getLandmarksNum();
            int oldBadCandidateSize = -1;
            do
            {
                loopCounter++;

                //System.out.println(loopCounter + " iteration on region " + i);
                virtualSystemSize = virtualSystemSizeSet((int) Math.pow(2, virtualSystemSizePower), i);
                if (virtualSystemSize > extremeVirturalSystemSize)
                {
                    virtualSystemSize /= 2;
                    break;
                }

                int[][] nameIDTable = virtualSystemNameIDTable(virtualSystemSize);


                Result result = ILP(nameIDTable, virtualSystemSize, getRepshare(i), i);
                accuracy = replicaSetGenerator4(result, virtualSystemSize, i, virtualSystemSize, dataOwnerIndex);
                if (accuracy == 0)
                {
                    System.out.println();
                }
                if (virtualSystemSizePower * accuracy > BestAccuracy)
                //if (accuracy >= BestAccuracy)
                {
                    BestAccuracy = accuracy * virtualSystemSizePower;
                    //BestAccuracy = accuracy;
                    //System.arraycopy(SubProblemRepSet, 0, BestSubProblemRepSet, 0, Simulator.system.getSystemCapacity());
                    System.arraycopy(RepSet, 0, BestRepSet, 0, SkipSimParameters.getSystemCapacity());
                    System.arraycopy(RepAccuracy, 0, BestRepAccuracy, 0, SkipSimParameters.getSystemCapacity());
                }

                refineBadCandidates(virtualSystemSize);

                if (accuracy < 0 || oldBadCandidateSize == badCandidates.size())
                {
                    break;
                }

                else if (badCandidates.size() >= (virtualSystemSize / 2) || badCandidates.size() == 0)
                {
                    if (badCandidates.size() > 0) oldBadCandidateSize = badCandidates.size();
                    badCandidates = expandBadCandidates();
                    virtualSystemSizePower++;
                }

                else //if(BestAccuracy == 1 &&(Math.abs(virtualSystemSize - badCandidates.size()) > (extremeVirturalSystemSize/2) || badCandidates.size() < (virtualSystemSize/2)) && badCandidates.size() > 0)
                {
                    break;
                }
                //if (accuracy > 0.99) virtualSystemSizePower++;

            } while (true);
            //while (virtualSystemSize < (double) Simulator.system.getSystemCapacity() / Simulator.system.getLandmarksNum());
            //while (Math.pow(2, virtualSystemSizePower) - badCandidates.size() < 128);
            //System.out.println("ILARAS terminated after " + loopCounter + " iterations for region " + i + " and stopped at " + virtualSystemSize + " total number of iterations till now " +
            //    numberOfIterations);


            for (int j = 0; j < SkipSimParameters.getSystemCapacity(); j++)
            {
                if (BestRepSet[j])
                {
                    if (((Node) sgo.getTG().mNodeSet.getNode(j)).getReplicaIDSet().contains(dataOwnerIndex))
                    {
                        System.out.println("Node " + j + " which selected by iLARAS as a replica has been already selected!");
                        System.exit(0);
                    }
                    repCounter++;
                    ((Node) sgo.getTG().mNodeSet.getNode(j)).setReplicaIDSet(dataOwnerIndex);
                    System.out.println("Name id " + ((Node) sgo.getTG().mNodeSet.getNode(j)).getNameID() + " belong to SkipGraph.Node " + j + " is selected as a replica");
                }

            }
            System.out.println("Sub-replication degree " + getRepshare(i));
            System.out.println("----------------------------------------------------------------------");


            //System.out.println(repStatus);
            //validityTest();
            //realWordTransform(i);
            //localReplicaSetInit();
        }
        numberOfIterations += (double) loopCounter / SkipSimParameters.getReplicationDegree();
        if (repCounter != SkipSimParameters.getReplicationDegree())
        {
            System.out.println("Error in the number of replicas MNR is " + SkipSimParameters.getReplicationDegree() + " but only " + repCounter + " replicas where made");
            System.exit(0);
        }
        System.out.println("Totally " + repCounter + " replicas where made");

        setCorrespondingReplica(dataOwnerIndex);
        double averageAccessDelay = 0;
        if (isPublicReplication()) averageAccessDelay = publicAverageDelay();
        else averageAccessDelay = privateAverageDelay();

        System.out.println("Virtual Simulator.system size power " + virtualSystemSizePower + "Average Delay " + averageAccessDelay + " Run " + SkipSimParameters.getCurrentTopologyIndex());
        return averageAccessDelay;

    }

    private ArrayList<String> expandBadCandidates()
    {
        ArrayList<String> newBadCandidates = new ArrayList<>();
        for (int i = 0; i < badCandidates.size(); i++)
        {
            newBadCandidates.add(badCandidates.get(i) + "0");
            newBadCandidates.add(badCandidates.get(i) + "1");
        }
        return newBadCandidates;
    }

    private double replicaSetGenerator4(Result R, int size, int landmarkIndex, int virtualSystemSize, int dataOwnerIndex)
    {
        SubProblemRepSet = new int[SkipSimParameters.getSystemCapacity()];
        RepSet = new boolean[SkipSimParameters.getSystemCapacity()];
        RepAccuracy = new double[SkipSimParameters.getSystemCapacity()];

        int virtualSystemNameIDSize = virtualSystemNameIDSize(virtualSystemSize);

        if (R == null)
        {
            return -Double.MAX_VALUE;
        }

        String result = new String();
        result = R.toString();

        double minAccuracy = Double.MAX_VALUE;

        for (int i = 0; i < size; i++)
        {
            String target = "Y" + i + "=1";
            if (result.contains(target))
            {
                int replicaIndex = findTheClosest3(i, landmarkIndex, virtualSystemNameIDSize, dataOwnerIndex);
                if (minAccuracy > RepAccuracy[replicaIndex]) minAccuracy = RepAccuracy[replicaIndex];
                SubProblemRepSet[replicaIndex] = i;
            }


        }

        return minAccuracy;
    }

    private int findTheClosest3(int k, int landmarkIndex, int virtualSystemNameIDSize, int dataOwnerIndex)
    {
        //System.out.println("Find the closest call with k = " + k + " and landmark = " + landmarkIndex );
        String s = Integer.toBinaryString(k);

        while (s.length() < virtualSystemNameIDSize) s = "0" + s;
        String prefix = sgo.getTG().mLandmarks.getDynamicPrefix(landmarkIndex);
        s = prefix + s;

        //System.out.println("Find the closest call with k = " + k + " and landmark = " + landmarkIndex);
        int maxCommon = 0;
        int maxIndex = 0;
        for (int i = 0; i < SkipSimParameters.getSystemCapacity(); i++)
        {
            if (s.equals(sgo.getTG().mNodeSet.getNode(i).getNameID()))
            {
                //System.out.println("Closest name id to " + s + " is " + sgo.getTG().mNodeSet.getNode(i).nameID );
                return i;
            }
            else if (commonBits(s, sgo.getTG().mNodeSet.getNode(i).getNameID()) >= maxCommon && !((Node) sgo.getTG().mNodeSet.getNode(i)).getReplicaIDSet().contains(dataOwnerIndex) && !RepSet[i])
            {
                maxCommon = commonBits(s, sgo.getTG().mNodeSet.getNode(i).getNameID());
                maxIndex = i;
                if (s.length() == maxCommon) break;
            }
        }

        double ac = (double) (commonBits(s, sgo.getTG().mNodeSet.getNode(maxIndex).getNameID()) - sgo.getTG().mLandmarks.getDynamicPrefix(landmarkIndex).length()) / virtualSystemNameIDSize;

        RepSet[maxIndex] = true;
        RepAccuracy[maxIndex] = ac;

        //System.out.println("Closest name id to " + s + " is SkipGraph.Node " + maxIndex + " with name id " + sgo.getTG().mNodeSet.getNode(maxIndex).nameID + " with " + (maxCommon - sgo.getTG().mLandmarks.getDynamicPrefix(landmarkIndex).length()) + " bits name id common prefix length, accuracy " + ac + " landmark prefix " + sgo.getTG().mLandmarks.getDynamicPrefix(landmarkIndex));
        return maxIndex;
    }

    private int virtualSystemNameIDSize(int virtualSystemSize)
    {
        int virtualSystemNameIDSize = 1;
        while (Math.pow(2, virtualSystemNameIDSize) < virtualSystemSize) virtualSystemNameIDSize++;
        return virtualSystemNameIDSize;
    }

    private int[][] virtualSystemNameIDTable(int virtualSystemSize)
    {
        int virtualSystemNameIDSize = 1;
        while (Math.pow(2, virtualSystemNameIDSize) < virtualSystemSize) virtualSystemNameIDSize++;
        int[][] vsNameIDTable = new int[virtualSystemSize][virtualSystemSize];
        for (int i = 0; i < virtualSystemSize; i++)
            for (int j = 0; j < virtualSystemSize; j++)
            {
                if (i == j)
                {
                    vsNameIDTable[i][j] = 0;
                }
                else
                    vsNameIDTable[i][j] = virtualSystemNameIDSize - commonBitsSubProblemSize(i, j, virtualSystemNameIDSize);
            }
        return vsNameIDTable;
    }

//    private ArrayList<Integer> refineBadCandidates(int virtualSystemSize)
//        {
//            ArrayList<Integer> badCandidates = new ArrayList<>();
//
//            int virtualSystemNameIDSize = 1;
//            while(Math.pow(2, virtualSystemNameIDSize) < virtualSystemSize)
//                virtualSystemNameIDSize++;
//
//            for (int i = 0 ; i < Simulator.system.getSystemCapacity(); i++)
//                {
//                    if(RepAccuracy[i] < 0.99 && RepSet[i])
//                        for (int j = 0; j < virtualSystemSize; j++)
//                            {
//                                if (commonBitsSubProblemSize(SubProblemRepSet[i], j, virtualSystemSize) >= virtualSystemNameIDSize * RepAccuracy[i])
//                                    {
//                                        badCandidates.add(j);
//                                    }
//                            }
//                }
//
//            return badCandidates;
//        }

    private void refineBadCandidates(int virtualSystemSize)
    {
        int virtualSystemNameIDSize = 1;
        while (Math.pow(2, virtualSystemNameIDSize) < virtualSystemSize) virtualSystemNameIDSize++;

        for (int i = 0; i < SkipSimParameters.getSystemCapacity(); i++)
        {
            if (RepAccuracy[i] < 0.99 && RepSet[i]) for (int j = 0; j < virtualSystemSize; j++)
            {
                if (commonBitsSubProblemSize(SubProblemRepSet[i], j, virtualSystemNameIDSize) > virtualSystemNameIDSize * RepAccuracy[i])
                {
                    badCandidates.add(toNameID(j, virtualSystemNameIDSize));
                }
            }
        }

        //System.out.print("Badcandidates ");
//        for (int i = 0; i < badCandidates.size(); i++)
//        {
//            System.out.print(badCandidates.get(i) + "   ");
//        }
//        System.out.println();

    }

    private String toNameID(int index, int virtualSystemNameIDSize)
    {
        String nameID = Integer.toBinaryString(index);
        while (nameID.length() < virtualSystemNameIDSize) nameID = "0" + nameID;
        return nameID;
    }

    private int commonBitsSubProblemSize(int i, int j, int virtualSystemNameIDSize)
    {
        String s1 = Integer.toBinaryString(i);
        String s2 = Integer.toBinaryString(j);
        while (s1.length() < virtualSystemNameIDSize) s1 = "0" + s1;
        while (s2.length() < virtualSystemNameIDSize) s2 = "0" + s2;
        int k = 0;
        while (s1.charAt(k) == s2.charAt(k))
        {
            k++;
            if (k >= s1.length() || k >= s2.length()) break;
        }
        return k;
    }

    private int virtualSystemSizeSet(int minSubProblemSize, int landmarkIndex)
    {

        int virtualSystemSize = Math.max(minSubProblemSize, getRepshare(landmarkIndex));

        int powerVirtualSystemSize = 2;
        while (powerVirtualSystemSize < virtualSystemSize && powerVirtualSystemSize < SkipSimParameters.getSystemCapacity())
            powerVirtualSystemSize *= 2;


        //
        // System.out.println("Virtual Simulator.system size of region " + landmarkIndex + " is set to " + powerVirtualSystemSize);

        return powerVirtualSystemSize;
    }

    private void printNameIDDistanceTable(int[][] L, int virtualSystemSize, ArrayList<Integer> badCandidateIndices, ArrayList<Integer> dataRequesters)
    {
        int virtualSystemNameIDSize = virtualSystemNameIDSize(virtualSystemSize);
        for (int i = 0; i < virtualSystemSize; i++)
        {
            System.out.print(toNameID(i, virtualSystemNameIDSize) + "    ");
            for (int j = 0; j < virtualSystemSize; j++)
            {
                if (badCandidateIndices.contains(i) || badCandidateIndices.contains(j)) System.out.print(" * ");
                else System.out.print((int) L[i][j] + "    ");
            }
            System.out.println();
        }
    }

    private ArrayList<Integer> dataRequesterIndices(int virtualSystemSize, int landmarkIndex)
    {
        ArrayList<Integer> dataRequesters = new ArrayList<>();
        if (isPublicReplication())
        {
            for (int i = 0; i < virtualSystemSize; i++)
            {
                dataRequesters.add(i);
            }
        }
        else
        {
            sgo.getTG().mNodeSet.updateClosestLandmark(sgo.getTG().mLandmarks);
            int virtualNameIDSize = virtualSystemNameIDSize(virtualSystemSize);
            for (int i = 0; i < SkipSimParameters.getDataRequesterNumber(); i++)
            {
                if (((Node) sgo.getTG().mNodeSet.getNode(i)).getClosetLandmarkIndex(sgo.getTG().mLandmarks) == landmarkIndex)
                {
                    String nameID = sgo.getTG().mNodeSet.getNode(i).getNameID();
                    String prefixFreeNameID = nameID.substring(nameID.length() - SkipSimParameters.getNameIDLength());
                    String squeezedNameID = prefixFreeNameID.substring(0, virtualNameIDSize);
                    int virtualSystemIndex = Integer.parseInt(squeezedNameID);
                    if (!dataRequesters.contains(virtualSystemIndex)) dataRequesters.add(virtualSystemIndex);
                }
            }
        }
        return dataRequesters;
    }

    private Result ILP(int[][] L, int size, int MNR, int landmarkIndex)
    {

        SolverFactory factory = new SolverFactoryLpSolve(); // use lp_solve
        factory.setParameter(Solver.VERBOSE, 0);
        factory.setParameter(Solver.TIMEOUT, Integer.MAX_VALUE); // set timeout to 100 seconds

        ArrayList<Integer> badCandidateIndices = new ArrayList<>();
        for (int i = 0; i < size; i++)
        {
            for (String nameID : badCandidates)
            {
                if (commonBits(nameID, toNameID(i, virtualSystemNameIDSize(size))) >= nameID.length() && !badCandidateIndices.contains(i))
                    badCandidateIndices.add(i);
            }
        }

        //System.out.println("ILP solver on size of " + (size - badCandidateIndices.size()));

        ArrayList<Integer> dataRequesters = dataRequesterIndices(size, landmarkIndex);
        //printNameIDDistanceTable(L , size, badCandidateIndices, dataRequesters);
        /**
         * Constructing a Problem:
         * Minimize: Sigma(i)Sigma(j) LijXij
         * Subject to:
         * for each i,j Yi>= Xij
         * Sigma(i)Xij = 1
         * Sigma(j)Xij >= Yi
         * Sigma(i)Yi <= MNR
         */

        Problem problem = new Problem();

        /**
         * Part 1: Minimize: Sigma(i)Sigma(j) LijXij
         */
        Linear linear = new Linear();
        for (int i = 0; i < size; i++)
        {
            if (badCandidateIndices.contains(i)) continue;
            for (int j = 0; j < size; j++)
            {
                if (badCandidateIndices.contains(j)) continue;
                if (!dataRequesters.contains(j)) continue;
                String var = "X" + i + "," + j;
                linear.add(L[i][j], var);
            }
        }
        problem.setObjective(linear, OptType.MIN);


        /**
         * Part 2: for each i,j Yi>= Xij
         */
        for (int i = 0; i < size; i++)
        {
            if (badCandidateIndices.contains(i)) continue;
            for (int j = 0; j < size; j++)
            {
                if (badCandidateIndices.contains(j)) continue;
                //if ((repType == Simulator.system.PRIVATE_REPLICATION && j >= Simulator.system.getDataRequesterNumber())) continue;
                linear = new Linear();
                String var = "X" + i + "," + j;
                linear.add(1, var);
                var = "Y" + i;
                linear.add(-1, var);
                problem.add(linear, "<=", 0);
            }

        }

        /**
         * Part 3: Sigma(i)Xij = 1
         */
        for (int j = 0; j < size; j++)
        {
            if (badCandidateIndices.contains(j)) continue;
            //if ((repType == Simulator.system.PRIVATE_REPLICATION && j >= Simulator.system.getDataRequesterNumber())) continue;
            linear = new Linear();
            for (int i = 0; i < size; i++)
            {
                if (badCandidateIndices.contains(i)) continue;
                String var = "X" + i + "," + j;
                linear.add(1, var);
            }
            problem.add(linear, "=", 1);
        }


        /**
         * Part 4: Sigma(j)Xij >= Yi
         */
        for (int i = 0; i < size; i++)
        {
            if (badCandidateIndices.contains(i)) continue;
            linear = new Linear();
            for (int j = 0; j < size; j++)
            {
                if (badCandidateIndices.contains(j)) continue;
                //if ((repType == Simulator.system.PRIVATE_REPLICATION && j >= Simulator.system.getDataRequesterNumber())) continue;
                String var = "X" + i + "," + j;
                linear.add(-1, var);
            }

            String var = "Y" + i;
            linear.add(1, var);
            problem.add(linear, "<=", 0);
        }


        /**
         * Part 5: Sigma(i)Yi <= MNR
         */
        linear = new Linear();
        for (int i = 0; i < size; i++)
        {
            if (badCandidateIndices.contains(i)) continue;
            String var = "Y" + i;
            linear.add(1, var);
        }
        problem.add(linear, "=", MNR);


        /**
         * Part 6: Sigma(j)Xij >= Yi
         */
        for (int i = 0; i < size; i++)
        {
            if (badCandidateIndices.contains(i)) continue;

            for (int j = 0; j < size; j++)
            {
                if (badCandidateIndices.contains(j)) continue;
                //if (repType == Simulator.system.PRIVATE_REPLICATION && j >= Simulator.system.getDataRequesterNumber()) continue;
                linear = new Linear();
                String var = "X" + i + "," + j;
                linear.add(1, var);
                problem.add(linear, ">=", 0);
                linear = new Linear();
                linear.add(1, var);
                problem.add(linear, "<=", 1);

            }

            linear = new Linear();
            String var = "Y" + i;
            linear.add(1, var);
            problem.add(linear, "<=", 1);

            linear = new Linear();
            var = "Y" + i;
            linear.add(1, var);
            problem.add(linear, ">=", 0);
        }


        /**
         * Part 6: Set the type of Xij and Yi
         */
        for (int i = 0; i < size; i++)
        {
            if (badCandidateIndices.contains(i)) continue;
            for (int j = 0; j < size; j++)
            {
                if (badCandidateIndices.contains(j)) continue;
                //if ((repType == Simulator.system.PRIVATE_REPLICATION && j > Simulator.system.getDataRequesterNumber())) continue;
                String var = "X" + i + "," + j;
                problem.setVarType(var, Integer.class);
            }

            String var = "Y" + i;
            problem.setVarType(var, Integer.class);
        }


        /**
         * Solving the problem
         */
        Solver solver = factory.get(); // you should use this solver only once for one problem
        //System.out.println(problem.toString());
        Result result = solver.solve(problem);


        //System.out.println(result.get("X30,30"));
        //System.out.println(result);

        //System.exit(0);
        return (result);
    }

    /**
     * A private class which is used to model the two dimentional key of form w1,w2
     */
    private class Key
    {

        private int index;
        private double accuracy;

        public Key(int x, int y)
        {
            this.index = x;
            this.accuracy = y;
        }


        public int getIndex()
        {
            return index;
        }

        public double getAccuracy()
        {
            return accuracy;
        }
    }
}