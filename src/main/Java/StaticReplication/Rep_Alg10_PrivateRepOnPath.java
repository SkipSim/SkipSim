package StaticReplication;

import Simulator.SkipSimParameters;
import SkipGraph.SkipGraphOperations;
import SkipGraph.Node;

import java.util.Arrays;
import java.util.Random;


public class Rep_Alg10_PrivateRepOnPath extends StaticReplication
{

    Random random = new Random();
    private boolean adaptiveReplication;

    public Rep_Alg10_PrivateRepOnPath(boolean adaptiveReplication)
        {
            this.adaptiveReplication = adaptiveReplication;
        }

    public void ReplicateOnPathGenerator(SkipGraphOperations sgo, int dataOwnerIndex)
        {
            int rep = 0;
            int dataOwner = random.nextInt(SkipSimParameters.getSystemCapacity() - 1);
            int dataRequerter = 0;
            while (rep <= SkipSimParameters.getReplicationDegree())
                {
                    dataRequerter = random.nextInt(SkipSimParameters.getDataRequesterNumber() - 1);
                    while (dataOwner == dataRequerter)
                        {
                            dataRequerter = random.nextInt(SkipSimParameters.getDataRequesterNumber() - 1);
                        }

                    //System.out.println(SkipGraph.Nodes.nodeSet.length + " " + Simulator.system.size + " " + searchDest);

                    rep = RepOnPath(sgo.getTG().mNodeSet.getNode(dataOwner).getNumID(), dataRequerter, rep, false, dataOwnerIndex);
                }

            setCorrespondingReplica(dataOwnerIndex);

        }

    private void AdaptiveReplicationOnPath(int dataOwnerID)
        {
            Arrays.fill(pathHistogram, 0);
            Random random = new Random();
            int dataOwner = random.nextInt(SkipSimParameters.getSystemCapacity());
            int searchDest = sgo.getTG().mNodeSet.getNode(dataOwner).getNumID();
            for (int i = 0; i < SkipSimParameters.getDataRequesterNumber(); i++)
                {
                    RepOnPath(searchDest, i, 0, true, dataOwnerID);
                }
            for (int i = 0; i < SkipSimParameters.getReplicationDegree(); i++)
                {
                    int MaxValue = pathHistogram[0];
                    int MaxIndex = 0;
                    for (int j = 0; j < SkipSimParameters.getSystemCapacity(); j++)
                        {
                            if (pathHistogram[j] > MaxValue)
                                {
                                    MaxValue = pathHistogram[j];
                                    MaxIndex = j;
                                }
                        }
                    ((Node) sgo.getTG().mNodeSet.getNode(MaxIndex)).setReplicaIDSet(dataOwnerID);
                    System.out.println(i + "-index = " + MaxIndex + " histogram " + pathHistogram[MaxIndex]);
                    pathHistogram[MaxIndex] = 0;
                }


            setCorrespondingReplica(dataOwnerID);

        }


    @Override
    public double Algorithm(SkipGraphOperations inputSgo, int dataOwnerIndex)
        {
            sgo = inputSgo;
            reset();
            double ratio = 0;
            //for (int i = 0; i < getExperimentNumber(); i++) {

            //repTools.replicaSetGenerator(repTools.PrivateOptimizer(repTools.realDistance, repTools.getProblemSize()), "Real", repTools.getProblemSize());

            if (adaptiveReplication) AdaptiveReplicationOnPath(dataOwnerIndex);
            else ReplicateOnPathGenerator(sgo, dataOwnerIndex);
            //double localDelay = privateTotalDelay();
            //int realDelay    = repTools.privateTotalDelay(repTools.realReplicaAssignment);
            double averageAccessDelay = privateAverageDelay();

            resetRep();

            //ratio = ratio + (double) localDelay; ///realDelay;
            //}

            //ratio = ratio / getExperimentNumber();
            //System.out.println("Average Latency " + ratio);
            //setRatioDataSet(Simulator.system.getCurrentTopologyIndex() - 1, ratio);
            //double averageAccessDelay = localDelay / Simulator.system.getDataRequesterNumber();
            System.out.println("Average Latency " + averageAccessDelay);
            setRatioDataSet(SkipSimParameters.getCurrentTopologyIndex() - 1, averageAccessDelay);

            if (SkipSimParameters.getCurrentTopologyIndex() == SkipSimParameters.getTopologyNumbers() && !SkipSimParameters.isDelayBasedSimulaton())
                {
                    //repEvaluation.loadEvaluation();
                    evaluation(" Algorithm 10 PrivateRepOnPath ");
                }

            return averageAccessDelay;

        }
}