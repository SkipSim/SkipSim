package StaticReplication;

import Simulator.SkipSimParameters;
import SkipGraph.SkipGraphOperations;
import SkipGraph.Node;

import java.util.Random;


public class Rep_Alg02_Random extends StaticReplication
    {

        private void randomReplicaGenerator(int dataOwnerID)
            {

                Random random = new Random();
                for (int i = 0; i < SkipSimParameters.getReplicationDegree(); i++)
                    {
                        int index = random.nextInt(SkipSimParameters.getSystemCapacity() - 1);
                        //int index = Math.abs(random.nextInt(Simulator.system.getSystemCapacity()) - random.nextInt(Simulator.system.getSystemCapacity()));
                        //int index = (int) (Math.random() * (Simulator.system.getSystemCapacity() - 1));
                        while (((Node) sgo.getTG().mNodeSet.getNode(index)).getReplicaIDSet().contains(dataOwnerID)) index = random.nextInt(SkipSimParameters.getSystemCapacity() - 1);
                        //index = Math.abs(random.nextInt(Simulator.system.getSystemCapacity()) - random.nextInt(Simulator.system.getSystemCapacity()));
                        //index = (int) (Math.random() * (Simulator.system.getSystemCapacity() - 1));

                        ((Node) sgo.getTG().mNodeSet.getNode(index)).setReplicaIDSet(dataOwnerID);
                        System.out.println(i + " index = " + index);
                    }


                setCorrespondingReplica(dataOwnerID);
            }

        @Override
        public double Algorithm(SkipGraphOperations inputSgo, int dataOwnerID)
            {
                sgo = inputSgo;
                System.out.println("Public Randomized Replication Started....");
                reset();
                //double ratio = 0;


                randomReplicaGenerator(dataOwnerID);
                //int localDelay = publicTotalDelay();
                double averageAccessDelay = publicAverageDelay();

                resetRep();


                //Real delay calculation
//            replicaSetGenerator(PublicOptimizer(realDistance, Simulator.system.getSystemCapacity()), "Real", Simulator.system.getSystemCapacity());
//            double realDelay  = publicTotalDelay() / Simulator.system.getSystemCapacity();
//            System.out.println("Real Delay is: " + realDelay);
//            ratio = (double)localDelay / realDelay;

                //double averageAccessDelay = localDelay/Simulator.system.getSystemCapacity();
                //System.out.println("Ratio " + ratio + " delay " + localDelay/Simulator.system.getSystemCapacity());
                System.out.println("Average Delay " + averageAccessDelay);
                setRatioDataSet(SkipSimParameters.getCurrentTopologyIndex() - 1, averageAccessDelay);

                if (SkipSimParameters.getCurrentTopologyIndex() == SkipSimParameters.getTopologyNumbers() && !SkipSimParameters.isDelayBasedSimulaton())
                    {
                        evaluation(" Algorithm 02 Random ");
                    }

                return averageAccessDelay;
            }

    }