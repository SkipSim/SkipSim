package StaticReplication;

import Simulator.SkipSimParameters;
import SkipGraph.Node;
import SkipGraph.SkipGraphOperations;

public class Rep_Alg03_RepOnNeighbors extends StaticReplication
    {

        public void ReplicateOnNeighborsGenerator(int dataOwnerIndex)
            {
                int index = 0;
                while (index < SkipSimParameters.getSystemCapacity() && ((Node) sgo.getTG().mNodeSet.getNode(index)).neighborNumber() < SkipSimParameters.getReplicationDegree()) index++;

                if (index >= SkipSimParameters.getSystemCapacity())
                    {
                        System.out.println("Error in Rep_Alg03_RepOnNeighbors.java: there is no SkipGraph.Node in the Simulator.system with " + SkipSimParameters.getReplicationDegree() + " neighbors" + "\n Change MNR please");
                        System.exit(0);
                    }

                for (int i = 0, rep = 0; i < SkipSimParameters.getLookupTableSize() && rep <= SkipSimParameters.getReplicationDegree(); i++)
                    for (int j = 0; j < 2 && rep <= SkipSimParameters.getReplicationDegree(); j++)
                        {
                            if (sgo.getTG().mNodeSet.getNode(index).getLookup(i, j) != -1)
                                {
                                    //realWorldReplicaSet[sgo.getTG().mNodeSet.getNode(index).getLookup(i, j)] = true;
                                    ((Node) sgo.getTG().mNodeSet.getNode(((Node) sgo.getTG().mNodeSet.getNode(index)).getLookup(i, j))).setReplicaIDSet(dataOwnerIndex);
                                    rep++;
                                }

                        }


//    	 for(int i = 0 ; i < Simulator.system.getSystemCapacity() ; i++)
//    	 {
//    		 int closestReplica = 0;
//    		 int closestReplicaDistance = Integer.MAX_VALUE;
//    		 for(int j = 0 ; j < Simulator.system.getSystemCapacity() ; j++)
//    		 {
//    			 if(sgo.getTG().mNodeSet.getNode(i).isReplicaIDSet())
//    				 if(sgo.getTG().mNodeSet.getNode(i).mCoordinate.distance(sgo.getTG().mNodeSet.getNode(j).mCoordinate) < closestReplicaDistance)
//    				 {
//    					 closestReplica = i;
//    					 closestReplicaDistance = (int) sgo.getTG().mNodeSet.getNode(i).mCoordinate.distance(sgo.getTG().mNodeSet.getNode(j).mCoordinate);
//    				 }
//    		 }
//
//    		 realWorldReplicaAssignment[closestReplica][i] = true;
//    	 }

                setCorrespondingReplica(dataOwnerIndex);

            }

        @Override
        public double Algorithm(SkipGraphOperations inputSgo, int dataOwnerIndex)
            {
                sgo = inputSgo;
                reset();
                double ratio = 0;


//			   replicaSetGenerator(PublicOptimizer(realDistance, Simulator.system.getSystemCapacity()), "Real", Simulator.system.getSystemCapacity());
//			   int realDelay = publicTotalDelay();
//			   resetRep();
                ReplicateOnNeighborsGenerator(dataOwnerIndex);
                //double localDelay = publicTotalDelay();
                double averageAccessDelay = publicAverageDelay();
                resetRep();

                //ratio = ratio + (double) localDelay / realDelay;
                //ratio = ratio / getExperimentNumber();
                //double averageAccessDelay = localDelay / Simulator.system.getSystemCapacity();
                System.out.println("Delay " + averageAccessDelay);
                setRatioDataSet(SkipSimParameters.getCurrentTopologyIndex() - 1, averageAccessDelay);

                if (SkipSimParameters.getCurrentTopologyIndex() == SkipSimParameters.getTopologyNumbers() && !SkipSimParameters.isDelayBasedSimulaton())
                    {
                        //repEvaluation.loadEvaluation();
                        evaluation(" Algorithm 03 RepOnNeighbors ");

                    }
                return averageAccessDelay;

            }
    }