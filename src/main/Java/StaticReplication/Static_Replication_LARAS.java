package StaticReplication;

import Simulator.SkipSimParameters;
import SkipGraph.SkipGraphOperations;
import SkipGraph.Node;
import net.sf.javailp.*;

import java.time.LocalTime;
import java.util.ArrayList;


public class Static_Replication_LARAS extends StaticReplication
{

    @Override
    public double Algorithm(SkipGraphOperations inputSgo, int dataOwnerIndex)
    {
        sgo = inputSgo;
        dataRequesterPopulation();
        populationBasedRepShareDefining();
        adaptivePopulationBasedSubproblemSizeDefining();

        double averageAccessDelay = regionWideOptimization(dataOwnerIndex);
        setRatioDataSet(SkipSimParameters.getCurrentTopologyIndex() - 1, averageAccessDelay);
        //repEvaluation.publicReplicationLoadAnalysis(repTools.getProblemSize(), repTools.realWorldReplicaAssignment);
        if (SkipSimParameters.getCurrentTopologyIndex() == SkipSimParameters.getTopologyNumbers() && !SkipSimParameters.isDelayBasedSimulaton())
        {
            //repEvaluation.loadEvaluation();
            evaluation(" LARAS ");
            System.out.println(LocalTime.now());
        }

        return averageAccessDelay;
    }


    private double regionWideOptimization(int dataOwnerIndex)
    {

        System.out.println("LARAS has started");

        for (int i = 0; i < SkipSimParameters.getLandmarksNum(); i++)
        {


            if (getRepshare(i) == 0) continue;


            int[][] nameIDTable = virtualSystemNameIDTable(getAdaptiveSubproblemSizes(i));


            Result result = ILP(nameIDTable, Math.max(SkipSimParameters.getReplicationDegree(), 16), getRepshare(i), i);
            replicaSetGenerator2(result, "Local", i, Math.max(SkipSimParameters.getReplicationDegree(), 16), dataOwnerIndex);
        }




        setCorrespondingReplica(dataOwnerIndex);
        double averageAccessDelay = 0;
        if (isPublicReplication()) averageAccessDelay = publicAverageDelay();
        else averageAccessDelay = privateAverageDelay();

        System.out.println("Average Delay " + averageAccessDelay + " Run " + SkipSimParameters.getCurrentTopologyIndex());
        return averageAccessDelay;

    }






    private int virtualSystemNameIDSize(int virtualSystemSize)
    {
        int virtualSystemNameIDSize = 1;
        while (Math.pow(2, virtualSystemNameIDSize) < virtualSystemSize) virtualSystemNameIDSize++;
        return virtualSystemNameIDSize;
    }

    private int[][] virtualSystemNameIDTable(int virtualSystemSize)
    {
        int virtualSystemNameIDSize = 1;
        while (Math.pow(2, virtualSystemNameIDSize) < virtualSystemSize) virtualSystemNameIDSize++;
        int[][] vsNameIDTable = new int[virtualSystemSize][virtualSystemSize];
        for (int i = 0; i < virtualSystemSize; i++)
            for (int j = 0; j < virtualSystemSize; j++)
            {
                if (i == j)
                {
                    vsNameIDTable[i][j] = 0;
                }
                else
                    vsNameIDTable[i][j] = virtualSystemNameIDSize - commonBitsSubProblemSize(i, j, virtualSystemNameIDSize);
            }
        return vsNameIDTable;
    }


    private String toNameID(int index, int virtualSystemNameIDSize)
    {
        String nameID = Integer.toBinaryString(index);
        while (nameID.length() < virtualSystemNameIDSize) nameID = "0" + nameID;
        return nameID;
    }

    private int commonBitsSubProblemSize(int i, int j, int virtualSystemNameIDSize)
    {
        String s1 = Integer.toBinaryString(i);
        String s2 = Integer.toBinaryString(j);
        while (s1.length() < virtualSystemNameIDSize) s1 = "0" + s1;
        while (s2.length() < virtualSystemNameIDSize) s2 = "0" + s2;
        int k = 0;
        while (s1.charAt(k) == s2.charAt(k))
        {
            k++;
            if (k >= s1.length() || k >= s2.length()) break;
        }
        return k;
    }



    private ArrayList<Integer> dataRequesterIndices(int virtualSystemSize, int landmarkIndex)
    {
        ArrayList<Integer> dataRequesters = new ArrayList<>();
        if (isPublicReplication())
        {
            for (int i = 0; i < virtualSystemSize; i++)
            {
                dataRequesters.add(i);
            }
        }
        else
        {
            sgo.getTG().mNodeSet.updateClosestLandmark(sgo.getTG().mLandmarks);
            int virtualNameIDSize = virtualSystemNameIDSize(virtualSystemSize);
            for (int i = 0; i < SkipSimParameters.getDataRequesterNumber(); i++)
            {
                if (((Node) sgo.getTG().mNodeSet.getNode(i)).getClosetLandmarkIndex(sgo.getTG().mLandmarks) == landmarkIndex)
                {
                    String nameID = ((Node) sgo.getTG().mNodeSet.getNode(i)).getNameID();
                    String prefixFreeNameID = nameID.substring(nameID.length() - SkipSimParameters.getNameIDLength());
                    String squeezedNameID = prefixFreeNameID.substring(0, virtualNameIDSize);
                    int virtualSystemIndex = Integer.parseInt(squeezedNameID);
                    if (!dataRequesters.contains(virtualSystemIndex)) dataRequesters.add(virtualSystemIndex);
                }
            }
        }
        return dataRequesters;
    }

    /*
    This ILP solver does not refines the bad candidates
     */
    private Result ILP(int[][] L, int size, int MNR, int landmarkIndex)
    {

        SolverFactory factory = new SolverFactoryLpSolve(); // use lp_solve
        factory.setParameter(Solver.VERBOSE, 0);
        factory.setParameter(Solver.TIMEOUT, Integer.MAX_VALUE); // set timeout to 100 seconds

        ArrayList<Integer> badCandidateIndices = new ArrayList<>();

        //System.out.println("ILP solver on size of " + (size - badCandidateIndices.size()));

        ArrayList<Integer> dataRequesters = dataRequesterIndices(size, landmarkIndex);
        //printNameIDDistanceTable(L , size, badCandidateIndices, dataRequesters);
        /**
         * Constructing a Problem:
         * Minimize: Sigma(i)Sigma(j) LijXij
         * Subject to:
         * for each i,j Yi>= Xij
         * Sigma(i)Xij = 1
         * Sigma(j)Xij >= Yi
         * Sigma(i)Yi <= MNR
         */

        Problem problem = new Problem();

        /**
         * Part 1: Minimize: Sigma(i)Sigma(j) LijXij
         */
        Linear linear = new Linear();
        for (int i = 0; i < size; i++)
        {
            if (badCandidateIndices.contains(i)) continue;
            for (int j = 0; j < size; j++)
            {
                if (badCandidateIndices.contains(j)) continue;
                if (!dataRequesters.contains(j)) continue;
                String var = "X" + i + "," + j;
                linear.add(L[i][j], var);
            }
        }
        problem.setObjective(linear, OptType.MIN);


        /**
         * Part 2: for each i,j Yi>= Xij
         */
        for (int i = 0; i < size; i++)
        {
            if (badCandidateIndices.contains(i)) continue;
            for (int j = 0; j < size; j++)
            {
                if (badCandidateIndices.contains(j)) continue;
                //if ((repType == Simulator.system.PRIVATE_REPLICATION && j >= Simulator.system.getDataRequesterNumber())) continue;
                linear = new Linear();
                String var = "X" + i + "," + j;
                linear.add(1, var);
                var = "Y" + i;
                linear.add(-1, var);
                problem.add(linear, "<=", 0);
            }

        }

        /**
         * Part 3: Sigma(i)Xij = 1
         */
        for (int j = 0; j < size; j++)
        {
            if (badCandidateIndices.contains(j)) continue;
            //if ((repType == Simulator.system.PRIVATE_REPLICATION && j >= Simulator.system.getDataRequesterNumber())) continue;
            linear = new Linear();
            for (int i = 0; i < size; i++)
            {
                if (badCandidateIndices.contains(i)) continue;
                String var = "X" + i + "," + j;
                linear.add(1, var);
            }
            problem.add(linear, "=", 1);
        }


        /**
         * Part 4: Sigma(j)Xij >= Yi
         */
        for (int i = 0; i < size; i++)
        {
            if (badCandidateIndices.contains(i)) continue;
            linear = new Linear();
            for (int j = 0; j < size; j++)
            {
                if (badCandidateIndices.contains(j)) continue;
                //if ((repType == Simulator.system.PRIVATE_REPLICATION && j >= Simulator.system.getDataRequesterNumber())) continue;
                String var = "X" + i + "," + j;
                linear.add(-1, var);
            }

            String var = "Y" + i;
            linear.add(1, var);
            problem.add(linear, "<=", 0);
        }


        /**
         * Part 5: Sigma(i)Yi <= MNR
         */
        linear = new Linear();
        for (int i = 0; i < size; i++)
        {
            if (badCandidateIndices.contains(i)) continue;
            String var = "Y" + i;
            linear.add(1, var);
        }
        problem.add(linear, "=", MNR);


        /**
         * Part 6: Sigma(j)Xij >= Yi
         */
        for (int i = 0; i < size; i++)
        {
            if (badCandidateIndices.contains(i)) continue;

            for (int j = 0; j < size; j++)
            {
                if (badCandidateIndices.contains(j)) continue;
                //if (repType == Simulator.system.PRIVATE_REPLICATION && j >= Simulator.system.getDataRequesterNumber()) continue;
                linear = new Linear();
                String var = "X" + i + "," + j;
                linear.add(1, var);
                problem.add(linear, ">=", 0);
                linear = new Linear();
                linear.add(1, var);
                problem.add(linear, "<=", 1);

            }

            linear = new Linear();
            String var = "Y" + i;
            linear.add(1, var);
            problem.add(linear, "<=", 1);

            linear = new Linear();
            var = "Y" + i;
            linear.add(1, var);
            problem.add(linear, ">=", 0);
        }


        /**
         * Part 6: Set the type of Xij and Yi
         */
        for (int i = 0; i < size; i++)
        {
            if (badCandidateIndices.contains(i)) continue;
            for (int j = 0; j < size; j++)
            {
                if (badCandidateIndices.contains(j)) continue;
                //if ((repType == Simulator.system.PRIVATE_REPLICATION && j > Simulator.system.getDataRequesterNumber())) continue;
                String var = "X" + i + "," + j;
                problem.setVarType(var, Integer.class);
            }

            String var = "Y" + i;
            problem.setVarType(var, Integer.class);
        }


        /**
         * Solving the problem
         */
        Solver solver = factory.get(); // you should use this solver only once for one problem
        //System.out.println(problem.toString());
        Result result = solver.solve(problem);


        //System.out.println(result.get("X30,30"));
        //System.out.println(result);

        //System.exit(0);
        return (result);
    }

    /**
     * A private class which is used to model the two dimentional key of form w1,w2
     */
    private class Key
    {

        private int index;
        private double accuracy;

        public Key(int x, int y)
        {
            this.index = x;
            this.accuracy = y;
        }


        public int getIndex()
        {
            return index;
        }

        public double getAccuracy()
        {
            return accuracy;
        }
    }
}