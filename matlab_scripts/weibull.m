k = 2.9;
lambda = 1.7;
x = 1:10;
PDF =  ((x/lambda).^(k-1)) .* exp(-((x/lambda).^k)) .* (k/lambda);
plot(x, PDF);

%CDF = 1 - exp(-(x/lambda).^k);
%plot(x, CDF);
% xlabel('time','FontSize',20);
% ylabel('probability','FontSize',20); 
% title(strcat('Session Length, lambda = ', num2str(lambda), ' k = ', num2str(k)));

