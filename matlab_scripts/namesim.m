function [] = namesim(t, MNR)
    %fid = fopen('C:\Users\Yahya Hassanzadeh\Google Drive\Java Workspace\NameSim\evaluationSDMeanResult.txt');
    fid = fopen('replicas_vs_time.txt');
    A = textscan(fid, '%f %f');
    x = A{1};
    y = A{2};
    plot(x,y);
    ylim([0 MNR])
    xlim([0 60480])
    title(t);
    xlabel('hours','FontSize',20);
    ylabel('number of replicas','FontSize',20); 
    fclose('all');
end

%namesim('cluster 128 t=42 days repTime=30 th day MNR =10', 10)